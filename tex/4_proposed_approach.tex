\section{Proposed Approach}\label{s:approach}

\subsection{Data Collection}

The data of \icitetitle{so.2019} is periodically published and available to downloaded at \url{https://archive.org/details/stackexchange}. There is a single \texttt{XML-file} for every entity type. An example thereof is shown in the code listing~\ref{lst:tagxml}. There are eight entity-types for which \texttt{XML-files} are available, namely
\begin{description}
    \item [Badges] ($\sim$ 29.000.000 items) Users can earn badges by completing certain achievements, e.g. answering a certain number of questions.
    \item [Comments] ($\sim$ 64.000.000 items) Contains all comments to questions and answers.
    \item [PostHistory] ($\sim$ 122.000.000 items) Contains all historic versions of questions and answers.
    \item [PostLinks] ($\sim$ 5.000.000 items) Posts can be linked, e.g. duplicates.
    \item [Posts] ($\sim$ 36.000.000 items) All the questions and answers.
    \item [Tags] ($\sim$ 54.000 items) Tags for the questions.
    \item [Users] ($\sim$ 10.000.000 items) All the users of the system.
    \item [Votes] ($\sim$ 162.000.000 items) The votes for questions, answers and comments.
\end{description}

\begin{lstlisting}[
    language=XML, 
    caption={\texttt{Tag.xml} structure of the \icite{so.2019} data.},
    label={lst:tagxml}
]
<?xml version="1.0" encoding="utf-8"?>
<tags>
  <row Id="1" TagName=".net" Count="286702" ExcerptPostId="3624959" WikiPostId="3607476" />
  <row Id="2" TagName="html" Count="844699" ExcerptPostId="3673183" WikiPostId="3673182" />
  <row Id="3" TagName="javascript" Count="1862833" ExcerptPostId="3624960" WikiPostId="3607052" />
  <row Id="4" TagName="css" Count="600232" ExcerptPostId="3644670" WikiPostId="3644669" />
  <row Id="5" TagName="php" Count="1303327" ExcerptPostId="3624936" WikiPostId="3607050" />
  <row Id="8" TagName="c" Count="309231" ExcerptPostId="3624961" WikiPostId="3607013" />
  <row Id="9" TagName="c#" Count="1339575" ExcerptPostId="3624962" WikiPostId="3607007" />
\end{lstlisting}

\subsection{Preprocessing}

First a MySQL database was created with the schema shown in figure~\ref{fig:dbschema}. Every \texttt{XML-file} was read, parsed, mapped and inserted into the database sequentially. A python program streams a single \texttt{XML-file} from the hard drive, instead of loading the whole file into memory, due to the small memory of the control machine. The data was then inserted into the respective tables in chunks. For this amount of data it took approximately 1 week of processing.

\image[
    caption=ER-Diagram created from the \iacite{so.2019} \texttt{XML-file} structure,
    label=fig:dbschema
]{images/er_diagram.png}

From the dataset most of the data was actually removed for these experiments. First the first 100.000 posts were extracted from the \emph{recs\_posts} table, excluding the posts from the user with the id $-1$, being \emph{the community user}. This is a special account, owned by process, is for example owning downvotes on spam, anonymous edit suggestions etc. and contributes just 15411 \emph{too much} compared to normal users. This user wrote X posts (questions and answers) has 324384 upvotes and 1093334 downvotes at the time the dataset was fetched. Afterwards all users belonging to the 100.000 posts were extracted. Both subsets were then imported in another MySQL instance and used for training. Afterwards the following 100.000 posts, skipping the ones belonging to users in from the training set, were exported and again the user belonging to those posts, to perform the tests.


\subsection{Development Environment}

\subsubsection*{Programming Language and Framework}

The language chosen was \href{https://www.python.org}{Python} in version 3.7.4. The framework was \href{https://www.djangoproject.com}{Django}. The decision for python is based on the fact that many popular \ac{ml} packages providing the algorithms from this study are written for python. Some of them are \hyperref[ss:gensim]{\itshape gensim}, \hyperref[ss:spaCy]{\itshape spaCy} and \hyperref[ss:scipy]{\itshape scipy}. Django was chosen for the familiarity of the author and for being a feature-rich framework. It provides an \ac{orm} and an interface to add commandline programs. The \ac{orm} was used to model the database objects used in this experiment, being the \emph{post} and the \emph{user} objects. Furthermore, three commandline programms where written for seeding the database from the \textrm{XML-files}, training the models and performing the tests.

\subsubsection*{spaCy}\label{ss:spaCy}

% TODO: reference spaCy

\icite{spaCy.2019} is an industrial-strength natural language processing framework for Python. It provides trained \ac{nlp} models for different languages in different sizes and with different methods. For this experiment the \emph{en\_core\_web\_lg} model is used.

\subsubsection*{gensim}\label{ss:gensim}

% TODO: reference gensim

\icite{gensim.2019} is a framework for topic modelling for Python. It provides the tools to train Word2Vec, Doc2Vec, \ac{lda}, \ac{tfidf} and others.

\subsubsection*{scipy}\label{ss:scipy}

\icite{scipy.2019} isa collection of open source software for scientific computing in Python and is used for the generation of plots in this study.

\subsection{Dataset Preparation}

To load the \texttt{XML-file} as a stream and iteratively parse the content, the python\\ \texttt{xml.etree.ElementTree} was used. If a \texttt{<row>} element was found, it was processed as described in figure~\ref{fig:seedpy}. It was important to transform the attribute names to \emph{snake\_case} and transform the datetime-values into python datastructures. Afterwards an \ac{orm} object of the entity type was created and filled with the attribute values. Although the file is parsed iteratively, line-by-line, inserting a new record in the database after each iteration would be slow due to the fact, that opening a database connection, or even reusing it, and parsing the SQL statement everytime, did in fact take too much time in the beginning. Hence, for this database seeding 300 objects were \emph{collected} and then inserted into the database with a bulk operation.

\image[
    caption=Process of the \texttt{seed.py} script,
    label=fig:seedpy,
    width=0.7\textwidth
]{images/seed_script.png}

\subsection{Building Item and User Models}

The models were trained exclusively on the bodies of the post objects. Item models were trained by being fed with post items while for the user models all the post bodies of the user's items were concatenated and considered as one long document. For the bodies to be usable they were preprocessed according to the process described in figure~\ref{fig:preprocess}. This process consists of normalisation by ensuring the text is in UTF-8 encoding; and cleaning the text from control sequences including \ac{html}. Thereafter the text is lower-case folded, \ac{pos}-tagged and then gradually hard-to-interpret characters and text is removed, e.g. whitespaces, punctuations, numbers, emails etc. The \ac{pos}-tags are utilised, by keeping only words with the lexical categories of \emph{noun, verb, adjective} and \emph{adverb}. Finally lemmatisation occurs, to increase the recall. The resulting corpus-documents are then tagged with the respective post id if its for the item model or with the user id otherwise.

\image[
    caption=Preprocessing steps for text,
    label=fig:preprocess,
    width=0.5\textwidth
]{images/preprocessing.png}

For this experiment the following text models were used to calculate similarity between objects:

\tabenv[
    label=t:text-models,
    caption=Displays the settings used to train the text-models Doc2Vec, \ac{tfidf} and \ac{lda}.
]{llllll}
{
\multicolumn{2}{c}{\textbf{Doc2Vec - \ac{dbow}}} & \multicolumn{2}{c}{\textbf{\ac{tfidf}}} & \multicolumn{2}{c}{\textbf{\ac{lda}}} \\ \toprule
settings & values & settings & values & settings & values \\ \midrule
vector size & 100 & term frequency & n (natural) & num topics & 50 \\
epochs & 10 & document frequency & t (idf) & passes & 1 \\
min count & 2 & normalisation & c (cosine) & eval every & 0 \\
dm & 0 &  &  &  &  \\
batch words & 1000 &  &  &  &  \\
sample & 0 &  &  &  &  \\
negative & 5 &  &  &  &  \\
hs & 0 &  &  &  &  \\ \bottomrule
}

Variants of the following item- and user models were generated for every text model type, resulting in 12 different models to be tested:

\begin{itemize}
    \item[]\textbf{item models}
    \begin{itemize}
        \item[\labelitemi] Trained only on posts that are questions
        \item[\labelitemi] Trained only on posts that are answers
    \end{itemize}
    \item[]\textbf{user models}
    \begin{itemize}
        \item[\labelitemi] For each user, concatenate their questions and feed it as one document to the model training
        \item[\labelitemi] For each user, that has at least 1 answer and 1 question, concatenate their answers and feed it as one document to the model training
    \end{itemize}
\end{itemize}

The activity diagram in figure~\ref{fig:pretrainpy} describes the training process. The algorithm iterates over all the text model types and only creates the model if it is not cached already. That was an important step during development, since the model generation code had changed many times and training for every model takes at least some hours. If the model does not exist, the model instance is first created and then trained according to the text model's training procedure, as Doc2Vec is trained differently to e.g. \ac{tfidf}.

\image[
    caption=Process of the \texttt{pretrain.py} script,
    label=fig:pretrainpy,
    width=0.5\textwidth
]{images/model_generator.png}

\subsection{Evaluating the Models}

The following recommendations are performed for every user to examine \textbf{[Hypothesis 1]}, \textbf{[Hypothesis 2]} and \textbf{[Hypothesis 3]} formulated to answer the research question.

\subsubsection{Similar Questions}

This recommender uses an item model to find the \emph{top-n} questions to every question the users themselves have posted and recommends them. The questions of the users themselves are excluded from the recommendations.

\subsubsection{Questions of Similar Users}

Here, for every user the \emph{top-n} similar users are retrieved via one of the user models, and then their questions are returned as recommendations.

\subsubsection{Questions Answered by Similar Users}

For every user the \emph{top-n} similar users are retrieved via one of the user models, and afterwards all the questions are fetched from the database that are answered by those similar users.

The experiment is performed for all combinations of the recommendation methods and the text models building the base of the user and item models.

After the recommendations for every user are generated a confusion matrix is generated according to the following rules
\begin{description}
    \item [true positive] recommended question was indeed already answered by the user
    \item [false positive] recommended question was never answered by the user
    \item [false negative] question was already answered by the user but was not recommended
    \item [true negative] all other questions
\end{description}

From this confusion matrix the following measures are calculated
\begin{multicols}{2}
\begin{itemize}
    \item accuracy
    \item precision
    \item f1-measure
    \item recall
\end{itemize}
\end{multicols}

They are then aggregated over all the recommendations to retrieve statistical information into

\begin{multicols}{3}
\begin{itemize}
    \item min
    \item max
    \item mean
    \item median
    \item standard deviation
    \item variance
\end{itemize}    
\end{multicols}
