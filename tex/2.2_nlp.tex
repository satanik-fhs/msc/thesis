

\subsection{Natural Language Processing}\label{ss:nlp}

Spoken and written language is part of human's history for thousands of years \iacite{russell.2009}. A \emph{natural language} is one that is used as the main means to communicate in any form and has evolved over the past, by passing it on through generations \iacite{bird.2009}. The field of analysing and transforming free-from text into a richer representation that can be utilised in digital systems is called \ac{nlp} \iacite{kao.2006}.

\icite{chowdhury.2003} refers to \ac{nlp} as "[\ldots] an area of research and application that explores how computers can be used to understand and manipulate natural language text or speech to do useful things."

As opposed to artificial languages, i.e. programming languages or mathematical notations \iacite{bird.2009}, natural languages consist of a lot of ambiguity and inconsistencies. Thus, different techniques to handling various problems have emerged \iacite{kao.2006}. Furthermore, different languages may have different rules or irregularities, which may need to be handled additionally \iacite{manning.2008}.

In the following sections a text unit will be called \emph{document} and a collection of documents will be referred as a \emph{corpus}.

\subsubsection{Tokenisation}

Tokenisation is used to split up a document into the smallest possible \emph{semantic units}. In the english and also the german language words are separated by whitespace and punctuation characters. Hence, tokenisers split off sentences at whitespace characters and usually remove punctuation characters.\\
In case of names consisting of compound words, e.g. \emph{Los Angeles}, the tokeniser has to know that this is a city name and that the city's meaning is lost if the two words are separated. The same could be said about phrases, hyphenated words, compound nouns or special kinds of tokens like email addresses, web links etc. \iacite{manning.2008}

\subsubsection{Stop-wording}

Looking at a document, some words usually contribute more to the meaning of this document than others. Some of the words carry almost no semantic value with them and removing them from the document before processing the rest, can improve performance.\\
Some of those words are conjunctions like \emph{and} and \emph{or}, pronouns like \emph{I} and \emph{you}, auxiliary words like \emph{to} etc.\\
In this case, since those words appear very often in documents and also in the whole corpus, it can save storage to strip them out and reduce processing time. This is especially true if the words are extensively post-processed after this step (augmentation, normalisation, lookups).\\
There are several edge cases however, in which leaving out those words changes the meaning. This happens for phrases or when the words carry information that is vital. Examples would be the \emph{to} in \emph{a flight to London} as opposed to \emph{a flight from London}, or the \emph{of} and \emph{the} in \emph{President of the United States}. For the latter a query search should result in more relevant results if the exact phrase is matched against the documents instead of only the single words.\\
In modern applications, like search engines etc., stop-word lists are not used anymore. Instead the words are weighted in a way, that \emph{tokens} that are of little value to the text have smaller scores, discussed in \cref{ss:document_scoring}. This enables applications to terminate early by ranking the tokens and if the scores get too low \iacite{manning.2008}.

\subsubsection{Normalisation}

For matching and searching in text, one expects the meaning to match and not the exact wording. One might for example write a search query as \emph{president of the us}. The documents may contain sentences that start with \emph{President of the}, having the word \emph{president} start with a capital letter. In this case one would expect the two terms still to match, that is why case-folding buy lower-casing the whole text. Like this all terms that are usually in lower case and only appear in the beginning of sentence with an upper case letter are normalised and still matched.\\
Furthermore the acronyms like \emph{US} for the \emph{United States} are also found. Acronyms though have more problems, including different forms of writing like e.g. \emph{U.S.A.} These and proper names, i.e. person and company names etc. should stay with their capitals to not match unnecessary words. An example of that would be the company \emph{General Motors} that when case folded most likely also matches terms like \emph{general} and different forms of \emph{motor}, which may not be intended.\\
To avoid those pitfalls, some heuristics do not case-fold the whole text but lower-case only a few words and keep everything as-is that is written in title-case or use more than one capitalised letter. Furthermore, models can be trained via \ac{ml} which uses more features and is known as \emph{true-casing}.\\
For the acronym dilemma of different writing forms and also diacritics, in e.g. na\"ive, equivalence classes can be used. For every word that has different forms, an equivalence class is created which is mapped to one form. In the aforementioned case all cases of $U.S.A., U.S., US \rightarrow US$ and $na\ddot{\imath}ve, naive \rightarrow naive$.\\
Additionally, it is also required to map other tokens including dates to map to a normalised form, so that \emph{March 23\textsuperscript{rd}} may be mapped to \emph{5/23} and so on \iacite{manning.2008}.

\subsubsection{Stemming and Lemmatisation}

Many words are using different forms for grammatical reasons but have the same meaning. So words in verb-form, nominalised or otherwise conjugated need to be mapped to some common term, for them to be still matched.\\
There are commonly two ways to achieve that. One is \emph{stemming} and the other is \emph{lemmatisation}. The former is a heuristic algorithm that is language-dependent, chopping of and transforming words according to a ruleset. In case of the \emph{Porter Stemmer} for the English language some rules consist of\\*

{
\centering
\begin{tabular}{>{$}l<{$}>{$}l<{$}>{$}l<{$}>{$}l<{$}}
\textbf{Rules} & & \qquad\qquad \textbf{Example} & \\
SSES &\rightarrow SS & \qquad\qquad caresses &\rightarrow caress \\
IES &\rightarrow I & \qquad\qquad ponies &\rightarrow poni \\
SS &\rightarrow SS & \qquad\qquad caress &\rightarrow caress \\
S &\rightarrow & \qquad\qquad cats &\rightarrow cat
\end{tabular}
\par
}

\hspace*{\fill}

\emph{Lemmatisation} on the other hand, is done by making use of a dictionary and a full morphological analysis of the language. Thereby only the inflections will be removed leaving the \emph{lemma} or dictionary form. The word \emph{saw} might be lemmatised to \emph{see} if it is a verb or \emph{saw} in case of being a noun.

Stemming yields a high recall and lowers precision, thus a problem arises when there is a lot of irregularities or the language has more morphology like in German. There \emph{lemmatisation} usually yields better results \iacite{manning.2008}.

\subsubsection{Synonyms}

Synonyms are different words or compounds having the same meaning. In the example of the word \emph{couch} it could be substituted with \emph{sofa}. Although this raises recall, like in stemming it also lowers precision. If the text contains \emph{Tim Couch} a replacement of \emph{Tim Sofa} would not be correct. The precision can, however, be improved with dictionary or statistical data. When a corpus contains multiple examples of the compound \emph{new couch} and \emph{new sofa}, this indicates that in that specific context, \emph{couch} can be safely replaced with \emph{sofa} \iacite{russell.2009}.

\subsubsection{Spelling Correction}

Text that is usually provided for informational purposes, like in books or descriptions are most of the time free from error. On the other hand, text from search queries, in forums etc. tend to contain a number of spelling mistakes occasionally. To reduce the sparsity of the data and improve the recall for matches spelling correction is needed. This can be divided in two different categories, namely \emph{isolated-word} and \emph{context-sensitive} correction. The former looks at each word separately and tries to correct the words, the latter corrects words depending on the context it is used in. While \emph{isolated-term correction} can be used to correct general spelling mistakes of words that may not even exist, the second helps especially in case a word itself is actually a proper word but makes no sense in this special context. Taking for example the search query \emph{carot}, corrections may be \emph{carrot} or \emph{tarot}. While in the query \emph{fly form Heathrow} the term \emph{form} is a proper word and hence, may not be corrected as an isolated term, but in the context of the aforementioned query an appropriate correction may be \emph{fly} \textbf{from} \emph{Heathrow}.

To correct \emph{isolated terms} one prominent method is using the \emph{edit- or Levenshtein distance}. It is a proximity measure for two different strings. Therefore, the distance is calculated as the \emph{edit operations} necessary to transform one string into another. The operations usually available are \emph{insertions, deletions} and \emph{substitutions}. The more operations needed the greater the distance between the two strings. The operations themselves could also be weighted, e.g. the substitution of two letters close on a keyboard could yield a lower distance, than when the keys are far away, or by using statistical data of common typing errors.\\
As it is not feasible to compare one string against all others of the corpus or a dictionary, normally heuristics are used. Such could consist of not changing the first letter -- assuming the first letter most of the time is not a mistake, computing distances only until a certain maximum threshold, or using permutations of the search term, since hitting letters in a reverse order, is a very common mistake.\\
A second method is the \emph{k-gram index}. For the search term all possible \emph{k-grams} will be computed and matching words with a subset of the same \emph{k-grams} will be returned. Oftentimes, this returns words that are not appropriate substitutions. Taking \emph{bord} as an example the \emph{bigrams} are \emph{bo}, \emph{or} and \emph{rd}. A word containing a subset of those is for example \emph{boardroom}. To avoid this, words that deceed for example a certain threshold of the \emph{Jaccard coefficient}, may be exempted from the results. This is calculated by taking the two sets of \emph{bigrams} from both compared strings, depicted as set $A$ and set $B$ by $|A \cap B| / |A \cup B|$. For the two aforementioned terms \emph{bord} and \emph{boardroom}, that would result in $2/(8+3-2)=0.\dot{2}$, dividing the number of matching \emph{bigrams} by all the \emph{bigrams} available in both words. Comparing \emph{bord} to \emph{board} meanwhile yields $2/(4+3-2)=0.4$, giving a better \emph{Jaccard coefficient}.\\
It would also be feasible to calculate the \emph{edit distance} of the words in the \emph{k-gram index}.\\
In case of proper nouns, they can be differently typed in different languages and still sound the same. For that, particularly \emph{phonetic correction} can be applied. This means, that for the term a phonetic hash will be generated, that should be the same if the pronunciation is the same. The hash will be generated by mapping similar sounding letters to the same character and normalising the result by padding and trimming to the same length. This class of algorithms is sometimes called \emph{soundex} algorithms.

\emph{Context-sensitive} spelling mistakes can be corrected by substituting each word in a search query by finding corrections for each single term. This would raise recall tremendously, but likewise decreases precision. A method that has proven very effective is using statistical data. To apply that candidate queries are generated by finding corrections for every word, but only the most used combinations are kept and used. Reusing the previous search query \emph{fly form Heathrow}, possible combinations would be \emph{fled fore}, \emph{flea form}, \emph{flew from} etc. The last one may be used most often in the corpus and thus is selected as the final correction \iacite{manning.2008}.

\subsubsection{Part-of-Speech Tagging}

\icite{argaw.2019} defines \ac{pos} tagging as "[...] the task of identifying and assigning the appropriate lexical category to words in sentences." Lexical categories include nouns, verbs, adjectives etc.

For computers to understand words within the context of a sentence, paragraph or document, the computer must not only know all the different meanings of a word, to draw a conclusion, it is also necessary to know the lexical category of a word. In the example from the previous section \emph{fly from Heathrow}, if the computer knew that the word is a verb, it would never accidentally believe it to be an animal. Furthermore, the computer can then understand, that \emph{Heathrow} is an object, i.e. a location, connected to the verb. Tagging words, however, is a non-trivial task, as even people tend to disagree on lexical categories sometimes. There are three ways to \ac{pos}-tag a document.

\begin{itemize}
    \item[] \textbf{rule-based:} Linguistic professionals design a complex ruleset that disambiguates tags for words. Then a dictionary is used to assign all the possible tags to words. Afterwards the words are fed into the rule-system and all the tags violating the ruleset should be dropped, leaving only one possible tag per word.
    \item[] \textbf{corpus-based:} In corpus-based approaches a training set is used to generate a \ac{pos}-tagging model, which is then used on unseen data.
    \item[] \textbf{hybrid:} Combination of rule-based and corpus-based approaches.
\end{itemize}

The rule-based approach can accurately identify \ac{pos}-tags for words, given, that the complexity of the language was completely captured and formulated in such a ruleset. For morphologically complex languages, though, this may take a long time for the experts to create. Furthermore, such rulesets can hardly be ported to other languages and languages also change over time. Corpus-based approaches contrarily, may result in under- or overfitted models, and may not capture the complexity of the language accurately \iacite{argaw.2019}.

\subsubsection{Document Scoring}\label{ss:document_scoring}

Section~\ref{ss:ir} talked about relevance for retrieving documents from a corpus matching a query. Relevance may be assigned manually by experts or computed. Document scoring is the field of assigning qualitative values to documents to allow ranking. The simplest form of determining the relevance of a document to a query is by how many words of the search query occur inside the document. As both the document and the search query may be written in natural language and contain errors, better results can be achieved by preprocessing both, the document and the query by the aforementioned methods of tokenising, stop-wording, normalisation, stemming or lemmatisation and \ac{pos}-tagging \iacite{manning.2008}.

\subsubsection*{Term Frequency}

Counting only the boolean values of occurrence of terms in a document, disregards the importance of some terms for the document (and the query respectively). To compensate that a well-studied method is using the \emph{term frequency} inside a document. Assuming a term is mentioned many times in a document its importance is elevated. In documents about cars, the word \emph{car} will occur more often than in documents about gardening. Thus, by implication, documents, in which the word \emph{car} occurs very often, may be about \emph{cars}. The \emph{term frequency} is denoted $tf_{t,d}$. While $t,d$ being the term and document in order. The score of a term can then be calculated as

\begin{equation}
    \vec{V}(d)=
    \begin{bmatrix*}
        tf_{t=0,d} \\
        tf_{t=1,d} \\
        tf_{t=2,d} \\
        \vdots \\
        tf_{t=N,d} \\
    \end{bmatrix*}
\end{equation}
\equationlistentry{Term-Frequency Vector}

$N$ being the number of different terms in the whole corpus, a feature vector $\vec{V}(d)$ for document $d$ can be constructed by counting the term frequencies inside a document, in which every component corresponds to the term frequency of a word occurring inside the document. The vector component for terms not present inside the current document consequently are $0$. Then the vectors can be used to calculate the similarity which will be discussed in the section \hyperref[ss:similarity]{\itshape Document Similarity} on page~\pageref{ss:similarity} \iacite{manning.2008}.

\subsubsection*{Inverse Document Frequency}

The term frequency score is not a good indicator on its own. Recalling the example with the term \emph{car} it may seem plausible that this yields suitable results, but many words will be frequent in some documents and are still not discriminative for that document. This is especially true for words that occur in many different or all documents. The conjunction \emph{and} for example may certainly be present in every document in the corpus multiple times. This shouldn't raise the document score though. On the contrary, a term used throughout the corpus should penalise the score. Even the word \emph{car} may be of little importance inside a document if it occurs in every document of the corpus, when the corpus itself is about cars for instance. This penalisation is done by using the \emph{document frequency}. Thus the term score is calculated according to equation~\ref{eq:tf-idf}.

\begin{equation}\label{eq:tf-idf}
    score_{t,d}=tf_{t,d}\times~idf_t=tf_{t,d}\times~\log{\frac{N}{df_t}}
\end{equation}
\equationlistentry{\acf{tfidf} Score}

$idf_t$ denotes the \emph{inverse document frequency}. And the document score becomes therefore:

\begin{equation}
    \vec{V}(d)=
    \begin{bmatrix*}
        score_{t=0} \\
        score_{t=1} \\
        score_{t=2} \\
        \vdots \\
        score_{t=N} \\
    \end{bmatrix*}
\end{equation}
\equationlistentry{\acf{tfidf} Vector}

Calculating the vectors for every document, they can be stored in the \emph{\ac{tfidf} matrix}. Rows correspond to the terms and columns to the document in the corpus \iacite{manning.2008}.

\subsubsection*{Document Similarity}
\phantomsection
\label{ss:similarity}

The similarity of two documents can be calculated by using the \emph{cosine similarity} of the two \ac{tfidf} vectors of the respective documents.

\begin{equation}
    sim(d1,d2)=\frac{\vec{V}(d1)\cdot \vec{V}(d2)}{\left \| \vec{V}(d1) \right \|\left \| \vec{V}(d2) \right \|}
\end{equation}
\equationlistentry{Cosine Similarity}

The \emph{dot product} returns the distance of the two vectors while the $L2-norm$ normalises the vectors to unit length. Therefore the similarity can be obtain as a value in the range $[0,1]$ \iacite{manning.2008}.

\subsubsection*{SMART Notation}

There are some alternatives for calculating document vectors. The SMART notation is very common in \ac{tfidf} to denote a specific scoring, denoted with a mnenomic triplet as $ddd$. The values such a triplet can have are defined in table~\ref{t:smart}.

\addtolength{\tabcolsep}{-3pt}  
\begin{table}[H]
\begin{tabular}{|lll|lll|lll|}
\hline
\multicolumn{3}{|l|}{Term frequency}                                                           & \multicolumn{3}{l|}{Document frequency}            & \multicolumn{3}{l|}{Normalisation}                                                                                  \\ \hline
n & (natural)   & $tf_{t,d}$                                                                   & n & (no)       & $1$                               & n & (none)                                                            & $1$                                         \\
l & (logarithm) & $1+log(tf_{t,d})$                                                            & t & (idf)      & $log\frac{N}{df_t}$               & c & (cosine)                                                          & $\frac{1}{\sqrt{w^2_1+\cdots +w^2_M}}$ \\
a & (augmented) & $0.5+\frac{0.5\times~tf_{t,d}}{max_t(tf_{t,d})}$                             & p & \begin{tabular}[c]{@{}l@{}}(prob \\ \ idf)\end{tabular} & $max\{0,log\frac{N-df_t}{df_t}\}$ & u & \begin{tabular}[c]{@{}l@{}}(pivoted \\ \ unique)\end{tabular} & $\frac{1}{u}$                               \\
b & (boolean)   & $\begin{cases}1 & \text{if}\ tf_{t,d} > 0 \\0 & \text{otherwise}\end{cases}$ &   &            &                                   & b & (byte size)                                                       & $\frac{1}{Charlength^\alpha}$    \\
L & (log ave)   & $\frac{1+log(tf_{t,d})}{1+log(ave_{t \in d}(tf_{t,d}))}$                     &   &            &                                   &   &                                                                   &                                             \\ \hline
\end{tabular}
\caption{SMART weighting adaptions. The notation is taken from the small letters in every column denoting the method to use for weighting and normalisation. From each column exactly one letter is chosen resulting in a triplet, e.g. \emph{ltc} yielding logarithmic term frequency, standard $idf$ and a cosine normalisation. \emph{CharLength} is the number of characters in the document and $\alpha\in [0,1]$ \iacite{manning.2008}.}
\label{t:smart}
\end{table}
\addtolength{\tabcolsep}{3pt}

It is common that for calculating the similarity different SMART combinations are used for the documents of the corpus and the search query, thus \emph{lnc.ntn} would be a valid configuration. In this case no \emph{idf}s are calculated for the corpus, saving performance \iacite{manning.2008}.

\subsubsection{Topic Analysis}

Topic analysis or topic modelling is the field of reducing a corpus dimensionality by approximating a documents vector in a lower dimensional space. As \ac{tfidf}- or more generally term-document-matrices are mostly huge because of the quantity of documents and terms, it is oftentimes computationally infeasible to work on the whole matrix. Topic analysis results in a model that can be trained on a randomly sampled subset of the corpus for performance reasons and new documents or search queries can be transformed into this lower dimensional space and similarities can again be computed as described in the section \hyperref[ss:similarity]{\itshape Document Similarity} on page~\pageref{ss:similarity}. Using this method, however, leads for the similarity computations to degrade over time, due to the fact, that new terms are not introduced and the model is not updated with new \ac{tfidf} values. Therefore, the model needs to be updated regularly if the performance of the \ac{irs} degrades too much.

\subsubsection*{Latent Semantic Indexing}

\ac{lsi} is a lower-rank approximation of a term-document matrix. This means that the term-document matrix, denoted as $C$, will be transformed from rank $r$ of the dimensions $M\times N$ to a lower rank matrix $C_k$ of rank $k$ resulting in the dimensionality $k\times N$. Consequently, the rows thereafter do not correspond to the terms anymore. To perform this transformation \ac{svd} is applied resulting in a configuration shown in equation~\ref{eq:svd}.

\begin{equation}\label{eq:svd}
    C=U\Sigma V^T
\end{equation}
\equationlistentry{\acf{svd}}

Here $U$ and $V$ are the $M\times M$ and $N\times N$ matrices whose columns are the orthogonal \emph{eigenvectors} of $CC^T$ and $C^TC$ respectively in that order. $\Sigma$ denotes the $M\times N$ matrix whose diagonal, i.e. $\Sigma_{ii}$, are the \emph{eigenvalues} of $C$, decreasing from $i=0$ to $i=r$.

To perform the low-rank approximation optimally a configuration should satisfy the \emph{Frobenius norm} of the matrix difference $X=C-C_k$ defined by the equation \ref{eq:frobenius}.

\begin{equation}\label{eq:frobenius}
    \left \|X\right \|_F=\sqrt{\sum^M_{i=1}\sum^N_{j=1}X^2_{ij}}
\end{equation}
\equationlistentry{Frobenius Norm}

Therefore the \ac{svd} is changed to:

\begin{equation}
    C_k=U\Sigma_k V^T
\end{equation}
\equationlistentry{Adapted \acf{svd} for lower-rank approximation}

$\Sigma_k$ is derived by setting the lowest ranking \emph{eigenvalues} to zero and afterwards computing $C_k$. $k$ should be chosen in the lower hundreds which results in a, surprisingly usually higher precision, as synonymy is addressed; a higher recall because generally more documents are retrieved and it uses a lot of resources to compute, so it should be applied to a randomly sampled subset as described in the previous section \iacite{manning.2008}.

\subsubsection*{Latent Dirichlet Allocation}

\ac{lda} is a generative model that generates documents from topics. In fact, it generates the words from topics. A \emph{topic} is a distribution over words and a document is a mixture of latent topics. The \ac{lda} process then generates the documents as follows:

\begin{enumerate}
    \item Choose document size $N\sim Poisson(\xi)$
    \item Choose the topic distribution $\theta\sim Dir(\alpha)$
    \item Iterate $N$ times\begin{enumerate}
        \item Choose a topic $z_n\sim Multinomial(\theta)$
        \item Choose a word $w_n\sim p(w_n|z_n,\beta)$
    \end{enumerate}
\end{enumerate}

$\alpha$ denotes the topic probabilities, $\beta$ the word probabilities and $Dir(\alpha)$ is the \emph{Dirichlet distribution function}. Turning this notion around, \ac{lda} can be used for model inference and parameter estimation. Therefore the hidden variables $k$ (number of topics), $\theta$ (topic distribution) and $z$ (word topic assignment distribution) have to be computed. This is done by computing the posterior distribution of the hidden variables. As this is intractable, inference approximation is a well studied problem. Examples are Laplace approximation, variational approximation and Markov Chain Monte Carlo techniques. For those the hidden variables are sampled iteratively for a fixed number of iterations or until convergence. Eventually, \ac{lda} yields a model, that can be used to tag documents with models according to the topics and distributions inferred and calculate a similarity thereof \iacite{blei2003latent}.


\subsubsection{Neural Embeddings}

\subsubsection*{Neural Networks}

\acp{ann} are borrowed from nature. Originally the human's nervous system consists of about $10^{11}$ neurons which are each connected to about $20.000$ other. A neuron transmits electrical or chemical signals, whereas the former are rare and are only used for hardcoded information. The latter are the actual neurons enabling organisms to learn. The core of a neuron is called a nucleus and has a resting potential of $-70mV$. By receiving ions from the post-synaptic dendrites, i.e. branches connected from previous neurons, to the current one, into the nucleus, the electrical potential rises until it reaches the threshold of $30mV$. Then channels in the membrane of the nucleus open up and the all the gathered ions pour out into the pre-synaptic axon of the current neuron, which in turn is again connected to many different dendrites of other neurons. This leaves the neuron with the initial resting potential of $-70mV$. The speed and throughput of electrical signals is controlled by the number of neurotransmitters sent out from one neuron to the other and the number of receptors for those neurotransmitters. Through the docking of the neurotransmitters, channels on the input side are opened to let the ions pour into the dendrites and consequently into the nucleus.

The concept of the nervous system is borrowed in the sense that an \ac{ann} consists of a 3-tuple $(N, C, w)$. $N$ is the set of neurons, $C$ the set of \emph{connections} and $w$ is the set of weights, controlling the \emph{speed} and \emph{throughput}, i.e. are the values adapted for learning. Typically \acp{ann} are not chaotic graphs, but rather a comprised of 2 or more layers. The minimum layers being the input layer and the output layer respectively. The connections $C$ are only logically in the sense, that the values passed on from one layer to the neurons of the next, are processed by a so called \emph{activation function}. An example of an activation function is shown in equation~\ref{eq:activation}.

\begin{equation}\label{eq:activation}
    f_{act}(X)=\sum_i wx_i
\end{equation}
\equationlistentry{Activation Function}

$X$ being the input vector to $f_{act}$. This activation function only passes the weighted values on, but it is conceivable to use a \emph{sigmoid-} or \emph{tanh-function} for probabilities or a mapping between $[-1;1]$ respectively. Layers between the input and the output layer of an \ac{ann}, are called \emph{hidden layers}. An \ac{ann} with many hidden layers is called a \ac{dnn}.

Given enough neurons and connections between them, an \ac{ann} may learn any function, thus they are also called \emph{general function approximators}. The process of learning for \acp{ann}, usually is supervised and happens through the following steps:
\begin{enumerate}
    \item the \ac{ann} weights are initialised randomly
    \item input data is sampled randomly from the training set and fed to the \ac{ann} through the input layer
    \item the \ac{ann} computes the values of the output layer
    \item the error is computed with respect to the \emph{ground truth} being the \emph{expectation}
    \item the weights are adapted with the gradient descent algorithm to minimise the error
    \item repeat from step 2. until convergence is reached
\end{enumerate}
To avoid overfitting, usually a regularisation parameter is used to decay the weights and penalise them during the updates. This way new values become increasingly important \iacite{Kriesel.2007, Aggarwal:2018}.

\subsubsection*{Word2Vec}

Word2Vec is a classifier, that tries to predict words from words. There are two different types of Word2Vec algorithms namely \ac{cbow} and skip-gram. The former tries to predict exactly \emph{one} word from a context of $2k$ words, while the latter is the inverse and tries to predict a context of $2k$ words from one input word.

The \ac{ann} structures for both are the exact inverse as well, so it is sufficient to describe only the structure of Word2Vec \ac{cbow} in the scope of this study. Let $d$ denote the number of unique words in the document. Furthermore, let $m$ be the context of $2k$ words. Then the input can be described as a \emph{one-hot encoded} vector $\bar{x}$ where $x_{ij}$ is the input from the $i^{th}$ group and the $j^{th}$ unique word therein. Thus, if $x_r$ is one of the input words, then the input of $x_{ir}$ for one of $m$ is $1$ and $0$ otherwise. This input layer is connected to a hidden layer denoted $h$ of dimensionality $p$. There is a weight $u_{ijq}$ from every neuron in the input layer to the hidden layer. The values are calculated as

\begin{equation}
    \bar{h}=\sum_i^m\sum_j^d \bar{u}_{j}x_{ij}    
\end{equation}
\equationlistentry{\acf{ann} hidden layer $\bar{h}$}

The activation function then is a softmax, mapping $\bar{h}$ to a $d$-dimensional standard $K$-simplex, defined as

\begin{equation}
    \Delta^K=\left \{ (x_0,\cdots,x_K)\in\mathbb{R}^K \biggr| \sum_k^Kx_k=0~and~\forall x_k\geq 0~and~1 \leq k \leq K \right\}    
\end{equation}
\equationlistentry{K-simplex}

The output layer is a vector $\bar{y}$ for which the result of the softmax function is weighted with the weight vector $\bar{v}$. The result is therefore

\begin{equation}
    \hat{y}=\sum_q^p\sum_j^d v_{qj}h_j    
\end{equation}
\equationlistentry{\ac{ann} output layer $\bar{y}$}

A schematic of such an \ac{ann} is shown in figure~\ref{fig:word2vec}.

\image[
    label=fig:word2vec,
    caption=Shows the architecture of the Word2Vec \ac{cbow} \ac{ann} \iacite{Aggarwal:2018}.,
    width=0.6\textwidth
]{images/word2vec-cbow.png}

To calculate the error the loss function

\begin{equation}
    L=-log(P(y_r=1|w_1...w_m))
\end{equation}
\equationlistentry{Negative log likelihood}

is the negative log likelihood, allowing an additive loss, which is more performant, than using the actual probabilities and the multiplicative loss. By backpropagating the network and adapting the weights to the loss-gradient one can minimise this negative log-likelihood \iacite{Aggarwal:2018}.

\subsubsection*{Doc2Vec}

Doc2Vec is an extension for which again two approaches are most common. Doc2Vec \ac{pvdm} is an extension of Word2Vec \ac{cbow} and Doc2Vec \ac{dbow} is an extension of Word2Vec skip-gram. In the architecture of Doc2Vec \ac{pvdm}, the input vector of the context words, is extended by a paragraph id. Therefore let the $(m+1)^{th}$ group be of the dimensionality $d'$, which is the number of paragraphs, and the input values $\bar{x}_{(m+1)}$ are \emph{one-hot encoded}. Moreover, there is a weight $u'_{(m+1)jq}$ from the extra group to each neuron of the hidden layer. The rest of the architecture is the same. For Doc2Vec \ac{dbow} the input is a $d'$-dimensional vector $\bar{x}$, fully connected to the hidden layer. The structures are shown in figure~\ref{fig:doc2vec} \iacite{Aggarwal:2018}.

\doubleimage[
    caption="Shows the architecture of the Doc2Vec \ac{pvdm} and \ac{dbow} \ac{ann} \iacite{Aggarwal:2018}.",
    captiona="Doc2Vec \ac{pvdm}",
    captionb="Doc2Vec \ac{dbow}",
    label=fig:doc2vec
]
{"images/doc2vec-dm".png}
{"images/doc2vec-dbow".png}