\section{Theoretical Background}\label{s:theory}

\subsection{Information Retrieval}\label{ss:ir}

\subsubsection{Information Need and Ground Truth}

\ac{ir} is the field of extracting relevant information from a set of \emph{documents} -- hereafter denoted as \emph{corpus}. Users of the \ac{irs} generally have some \emph{information needs} and input queries they deem to best match this information need. The \ac{irs} then processes this query and returns a set of relevant documents from the corpus. The information need may, however, not always be inferred easily from the query, especially if the query is very imprecise. Suppose a user queries the \ac{irs} for \texttt{"python"}. The information need may be constituted of information about the snakes or the programming language. In this case it is not apparent and the retrieved documents have to be judged to the information need. The decision which documents are relevant to the information need is called the \emph{ground truth} \iacite{manning.2008}.

\subsubsection{Relevance Classification}

The \ac{irs} needs to take the query, infer the information need and then classify the documents in the corpus accordingly, to return only documents classified as \emph{relevant} to the user. Text classification can be discrete or continuous. So a document may have just the nominal binary tags \emph{relevant/irrelevant}, may include ordinal information such as \emph{highly relevant} or a measure of its relevance obtained by calculation as an interval or ratio \iacite{manning.2008}.

\subsubsection{Evaluation of Unranked Retrieval Sets}

In an unranked retrieval set of an \ac{irs} all documents have the same relevance. There is no order, rank or difference between to documents in terms of their relevance. An \ac{irs} can be evaluated by calculating certain measures. The most prominent are \emph{accuracy, precision, recall} and the \emph{F-measure}. To obtain these a \emph{confusion matrix} has to be calculated first  \iacite{manning.2008}.

\subsubsection*{Confusion Matrix}

A confusion matrix is a table that presents the data with a measure in four categories. The nominal information in the columns is the \emph{relevance} and the rows denote the \emph{ground truth}. Table~\ref{t:confusion_matrix} shows an example of a confusion matrix with a ground truth of 3 relevant documents, 10 irrelevant documents and a retrieval of 5 documents by the \ac{irs} of which 3 are actually not relevant according to the information need. The values denoted as follows \emph{\ac{tp}, \ac{tn}, \ac{fp}} and \emph{\ac{fn}}  \iacite{manning.2008}.

\begin{table}[H]
\centering
\caption{Example of a confusion matrix with 2 relevant documents correctly retrieved, 3 falsely, and one classified as irrelevant even though it is relevant according to the \emph{ground truth}}
\label{t:confusion_matrix}
\begin{tabular}{|l|l|l|} 
\cline{2-3}
\multicolumn{1}{l|}{} & positive & negative  \\ 
\hline
relevant                  & 2 (TP)        & 1 (FN)         \\ 
\hline
irrelevant                & 3 (FP)        & 7 (TN)         \\
\hline
\end{tabular}
\end{table}

\subsubsection*{Accuracy}

Accuracy is the fraction of correctly classified documents. Thus, the accuracy measure can be obtained by equation~\ref{eq:accuracy}.

\begin{equation}\label{eq:accuracy}
    Accuracy = \frac{\ac{tp}+\ac{tn}}{\ac{tp}+\ac{fp}+\ac{tn}+\ac{fn}}
\end{equation}
\equationlistentry{Accuracy Measure}

Accuracy, however, is most of the time not a good measure to evaluate classification, because most corpora contain a very diverse set of documents, thus most of the documents should be irrelevant for any query. Therefore, when the \ac{irs} retrieves only a fraction of the corpus, it means, regardless of the quality of the classification of the retrieval, accuracy would be very high. That can be shown very quickly by assuming an \ac{irs} retrieved 0 \ac{tp} and 10 \ac{fp} of one million documents. This results in an accuracy of $0.99999$, which looks as if the \ac{irs} performed very well, but only on the non-retrieved items, which tend to be more important to the user  \iacite{manning.2008}.

To address this issue better performance measures are \emph{precision}, \emph{recall} and the \emph{f-measure}.

\subsubsection*{Precision and Recall}

Precision is the fraction of how many of the retrieved documents are relevant, while recall is the fraction of how many of the relevant documents are retrieved. Equation~\ref{eq:precision} and \ref{eq:recall} show the formulas respectively.

\begin{equation}\label{eq:precision}
    Precision = \frac{\ac{tp}}{\ac{tp}+\ac{fp}}
\end{equation}
\equationlistentry{Precision Measure}

\begin{equation}\label{eq:recall}
    Recall = \frac{\ac{tp}}{\ac{tp}+\ac{fn}}
\end{equation}
\equationlistentry{Recall Measure}

When plotting precision against recall it is overt, that there is a trade-off to be made. For every \ac{ir} task either precision or recall are more important. Recall is a monotonically increasing measure, that rises the more documents are retrieved until the number of relevant items is reached. Precision on the other hand drops every time an irrelevant document is retrieved. Figure~\ref{fig:precision_recall} shows an example of precision-recall plot which shows, that the more recall increases the lower precision becomes \iacite{manning.2008}.

\image[
    caption=Precision against Recall with a saw-tooth shape \iacite{manning.2008}.,
    label=fig:precision_recall,
    width=.6\textwidth
]{images/precision-recall.png}

\subsubsection*{F-measure}

The \emph{F-measure} is the weighted harmonic mean of precision and recall:

\begin{equation}
    F=\frac{1}{\alpha\frac{1}{P}+(1-\alpha)\frac{1}{R}}=\frac{(\beta+1)PR}{\beta~P + R} \textnormal{\qquad~where\qquad} \beta=\frac{1-\alpha}{\alpha}
\end{equation}
\equationlistentry{F-Measure}

The harmonic mean is a better measure than the arithmetic mean, simply because one can always get $100\%$ recall by returning all documents and therefore would get at least $50\%$ for the arithmetic mean. The \emph{default balanced F-measure} equally weights both, precision and recall by setting $\alpha = 0.5$ or $\beta = 1$ resulting in the $F_{\beta=1}$- or simply $F_1$-measure \iacite{manning.2008}.

\begin{equation}
    F_{\beta=1}=\frac{2PR}{P+R}
\end{equation}
\equationlistentry{F1-Measure}

\subsubsection{Evaluation of Ranked Retrieval Sets}

In case the \ac{irs} uses a non-binary relevance classification, either by using ordinal tags or some continuous relevance measure, the relevant documents can be ranked and it is also feasible to only return the $k$ top-ranked documents. A search engine for instance returns ranked documents paged in chunks. It is possible to calculate accuracy, precision, recall and F-measure for the $k$ top-ranked documents with an increasing $k$. This way a precision-recall plot can be obtained as shown in figure~\ref{fig:precision_recall}.