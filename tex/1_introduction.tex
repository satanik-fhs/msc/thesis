\section{Introduction}\label{s:intro}

\subsection{Background}

Data online and in software services increases continuously. This leads to an abundance of data, which is hard to process and filter manually by the end-user. Hence, the need for personalisation and customisation, as a means to filter the data to give the interested party what they desire instead of overwhelming them, increases.

\icite{chellappa.2005} defines personalisation as "[...] the ability to proactively tailor products [...] to tastes of individual consumers based upon their personal and preference information". On the other hand, customisation is the process of detecting the users interests and delivering according to them at almost as efficiently as one would in mass production \iacite{tseng.2003}. Meaning, that for personalisation, the system understands the user and provides the filtered content, while for customisation the users need to provide their information, for their content to be filtered.

It is not feasible anymore to provide personalisation and customisation manually. Online services, which have potential user base of thousands or millions, can only tackle this problem by using appropriate automatisation systems. For that to work, however, the systems are required to \emph{understand} the data available to them. This includes them being able to parse the data, extract information, interpret this information, draw conclusions and act upon those.

Applications leveraging the need for personalisation rely heavily on items that can primarily be described with structured information. Those items are easily categorised within different dimensions and can, given enough features, be uniquely identified therewith. Prime examples of such items are books \iacite{Mooney:2000:CBR:336597.336662,Pera:2015:ABF:2700171.2791037}, movies \iacite{falk2019practical}, goods \iacite{Linden.2003,Frey2015ANR}, cars \iacite{MIAO2007397, 1181672} etc.

Regarding goods, because the class is the most diverse from the aforementioned, those can still be classified by
\begin{itemize}
    \item \textbf{category} (kitchen, wearables, electronics), even in \textbf{subcategories} understood by most (games, porcelain, smart watches),
    \item \textbf{price}, which at a certain point in time is fixed and can be paid via the same method for all available goods presented,
    \item \textbf{seller}, direct or indirect,
    \item \textbf{additional properties}, inside the same subcategory, like screen size, measurements, color, ports available etc.,
    \item and most importantly the \textbf{name}.
\end{itemize}

This defines a finite space, for which similarity can be computed between items.\\
There are problem domains however, in which one item cannot be easily distinguished taking only meta data into account. Examples of those generally carry the most information in free-form text, that is supplied by people manually, as classification is an inefficient method, weighing the effort against the gains. Prominent examples are news \iacite{Garcin:2014:OOE:2645710.2645745,kompan:2010}, events \iacite{Khrouf:2013:HER:2507157.2507171,2040145,Arora:2016}, service market places \iacite{thumbtack.2018} and other text-centered internet services like blogs, document management software, social media etc.

Thinking of e.g. Twitter, it would not make sense to add a hashtag for every discerning word inside the message. This would create a precise tagging of every message, but also increase the search space infinitely and thus increase the computational resources needed. The same applies to events, which tend to have a certain number of hard-coded meta tags such as location, start-, end-times, host, category. But again, only considering these, makes it hard to differentiate between two events, e.g. a comedy show by one person and another. If you could reach both locations easily and they are set at the same time, which would you choose over the other? You would need to look at the description and decide according to the information you filter out yourself.

Moreover, there arise several problems with non-standardised taxonomies like categories. Taking service marketplaces as an example, the Austrian Economic Chambers \iacite{WKO:2019:Online}, keep a list of service sectors they have identified for themselves and many applications respect those. Even then, if new types of services are created they first need to be categorised, on the other hand users of such applications may not be familiar with the list of the WKO and their mental model deviates, which makes it harder to use this predefined list. Furthermore, even if the user's idea aligns with the system's, filter mechanisms may still not correctly find the right suggestions.

Currently these applications mostly use customisation methods like search, manual filtering and categorisation by whatever meta information is available.

The aforementioned problems, lead to the hypothesis, that a smartly designed, flexible, though robust, personalisation system, that automatically adapts to the user base and the content, can tackle these problems head on, while having the advantage, that the users do not have to invest any time on an unintuitive setup and complex customisation.

Still the personalisation approaches have one not negligible perquisite: \emph{data}. Most small applications have not a lot of initial data or the data does not exist in an adequate quality, while even bigger ones, face the problem of user and item \emph{cold-start} \iacite{schein.2002, barjasteh.2015}. This means, that there is no information available to infer any interest information from either newly registered users or newly created items.

\subsection{Problem Statement}

Question- and answer \acused{qna} (\ac{qna}) systems are prominent on the web. Those are knowledge exchange platforms for a wide variety of fields, spanning from common topics, over medicine to technical or fan-content. Finding the answer to a certain question is in most cases simply solved, by utilising a search engine, which has the \ac{qna} system pages already indexed and yields the respective results.

On the other hand, finding questions to answer them, is a non-trivial problem, assuming, that the user has a reason to answer questions, a query to a search engine may only prove successful if the user knows in advance which question should be answered.

Otherwise, if one tries to answer questions for the primary sake of answering, a few requirements have to be met, to even narrow down the content available to find questions, that can be answered, as having the appropriate knowledge, being able to answer better than other's, answering before another gives a sufficient answer, answering maybe not only the specific question but in a more generalised way, so that the answer can be applied to similar questions etc.

A well-known platform is \icite{StackExchange:2019:Online}. This platform consists of several sub-platforms for specialised content. Examples are \emph{\icitetitle{so.2019}}, \emph{\icitetitle{MathStackExchange:2019:Online}}, \emph{\icitetitle{TexStackExchange:2019:Online}} etc. The experiment of this work is conducted using a subset of the data from \icitetitle{so.2019} from the 10\textsuperscript{th} of September 2019.

\subsection{Stack Overflow}\label{ss:stackoverflow}

\subsubsection{About}

\icite{so.2019} is the largest and most trusted question and answer site to exchange knowledge and help programmers solve coding problems all around the world, by letting the community post questions and detailed answers \iacite{so.2019}.

\image[
    caption=StackOverflow Homepage \iacite{so.2019},
    label=fig:so
]{images/stackoverflow-home.png}

\subsubsection*{Questions}

The most important interactions with the platform are asking questions and answering them. This is the most basic functionality and every user may do both. To ask a question the user initiates this by clicking the blue \emph{Ask Question} button shown in fig \ref{fig:so}. This opens a form to fill out the details about the question to ask. It includes a title, a body of free-form text and an input field for tags. As long as the questions align with the code of conduct of \icite{StackExchange:2019:Online} (no self-promotion, avoiding duplicates etc.), there are no restrictions on the content of the questions. Questions should contain information as specific as possible and exactly as much information to be answered by others. Thus, unnecessary information should be avoided and everything to understand the context of the problem, should be included (e.g. logs, code examples). Thereafter, the question is online and can be found and answered by anyone, even the authors themselves.

\image[
    caption=StackOverflow Answered Question \iacite{so.2019},
    label=fig:so:answered
]{images/stackoverflow-answered.png}

\subsubsection*{Answers}

Questions can be answered multiple times, by different people. The answer form only consists of the free-form text editor. Answers should follow the same rules as the questions, regarding the code of conduct, specificity and conciseness. An answer may be accepted by the authors of the question, when they deem the answer appropriate to solve the problem.

\subsubsection*{Comments}

Questions and answers are not the only entities available on \icitetitle{so.2019}. Users may also write comments to either questions or answers; for clarification, correction suggestions, remarks etc. Oftentimes, these are the basis for discussion about either items. When for instance a question is not clear enough for the community to answer, the comments can be used to ask for additional information. On the other hand, if an answer is outdated, misses the point, is not complete or otherwise insufficient, the community can point that deficiency out and allow the author of the answer to act upon it.

\subsubsection*{Votes}

Additionally, questions, answers and comments can be up- and downvoted. This is an indicator of the interest in the item or the quality of it. Good questions and answers tend to receive a lot of upvotes, whereas bad ones usually result in downvotes.

\subsubsection*{Tags}

As previously mentioned, questions are accompanied by tags. Tags are any distinguishable keywords or labels best categorising the question. Those may be pronouns of programming languages, frameworks, technology, methods etc. It is highly recommended, however, to choose well established tags and ones that are commonly used. There is an upper limit of five tags per question.

\subsubsection{User Goals}

Users may use \icitetitle{so.2019} for different reasons. The main reasons are finding solutions for problems, providing a solution for a problem and to contribute to the community. Asking questions and answering some, is actually a form of contribution already, as it expands the knowledge database of \icitetitle{so.2019}. If however, users want to get more involved, they first have to earn the respective privileges, like being able to up- and downvote, creating wiki posts and chatrooms, commenting everywhere, editing others answers and questions, creating tags, deleting posts, access to site analytics etc.

If for some reason, a user wants one of those privileges, reputation has to be accumulated. Gaining reputation is possible by receiving upvotes, getting answers accepted, accepting answers, suggested edits are accepted, bounties are awarded etc. On the contrary it is also possible to loose reputation by receiving downvotes on one's questions, answers, comments, by downvoting themselves, giving out bounties etc. This also incites all users to use the platform as intended.

\subsubsection{Reputation Acquisition}

Although it is possible to receive reputation by asking good questions and getting upvotes, it is non-trivial to come up with questions. Therefore, it is easier to \emph{hunt} for reputation by answering questions. The platform currently requires the user to actively query for questions one wants to answer by providing some tools, which are
\begin{itemize}
    \item menu items, allowing quick access to specific queries e.g. tags, users,
    \item featured buttons, for actuality (hot, week, month), interesting, with bounties,
    \item links, on items for tags, users and references to other items,
    \item search bar, with a specialised query language.
\end{itemize}

The most effective way is to utilise the query language provided, which allows filters such as \textbf{\emph{"[javascript] [angular] hasaccepted:yes created:1m"}}, which would result in a recommendation of all questions, created in the last month, that have at least one accepted answer, tagged with \emph{javascript} and \emph{angular}.

To prove that personalisation can replace customisation systems, improving the user's perceived ease of use and user experience by stripping away the error-prone tasks user's would need to perform otherwise, the following research question is formulated.

\begin{itemize}
    \item[] \textbf{[Research Question]:} Can personalisation approaches yield similar quality suggestions to customisation systems for \emph{question-and-answer-sites}?    
\end{itemize}

\subsection{Objectives}

The general objective of this study is to provide a method for \ac{qna} systems to grow faster in size and quality by alleviating the obstacles of active manual searching and filtering for interests. Furthermore, finding one concrete or a combination of recommendation methods that yield suitable results for the \ac{qna} site.

\subsection{Methodology}

The methodology to answer the aforementioned research question and achieving the objectives includes the following tasks.

\textbf{Literature review:}

\textbf{Data collection:} The data is acquired by the \icitetitle{StackExchange:2019:Online} archive and transformed from an \texttt{XML-format} into a relational database. 

\textbf{Preprocessing:} The data, especially the free-form text of the posts, will be preprocessed with \ac{nlp} to reduce noise and the feature vector space.

\textbf{Building item and user models:} Item and user similarity models are generated for every type of \ac{nlp}/text model.

\textbf{Evaluating the models:} The models are first evaluated by looking at the self-similarity between users and items and how high they are ranked to themselves compared to the other users and items.

\textbf{Generating recommendations:} A number of recommendation methods are used in succession on the same data resulting in a confusion matrix. The experiment will be performed on the training data, as well as on the test data.

\textbf{Evaluating the recommendations:} From the confusion matrix for every recommendation different classification measures can be derived, i.e. precision, recall, accuracy. These are aggregated and plotted for further analysis.

\subsection{Scope of the Study}

This study is exclusively using a small subset of the \icitetitle{so.2019} dataset, namely only from the \emph{posts} and \emph{users} data. Using a larger subset or additional data, i.e. tags, postlinks, history, votes and comments, may improve the results but these are out of scope and may be part of future work.

\subsection{Contributions}

This study is contributing by proving that \ac{qna} sites benefit clearly from an automated recommendation system, which would improve the user experience of the contributors trying to answer questions, by providing a simpler process, which requires less effort and reduces the hurdle of completing this task.

\subsection{Thesis Organization}

The thesis is organised as follows. Section~\ref{s:theory} will discuss various methods to realise customisation  and personalisation necessary to understand the concepts used throughout the following sections, primarily explaining the theory behind \ac{nlp} and \ac{rs}. Section~\ref{s:literature} presents related previous work and the state-of-the-art of \ac{rs} with and without utilising \ac{nlp}. Section~\ref{s:approach} will focus on the approach proposed in this thesis. Section~\ref{s:experiment} describes the experiment conducted and the results obtained thereby. Section~\ref{s:conclusion} presents the conclusion and outlook.
 
 
 
 
 
 
 
 
 
