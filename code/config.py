import os

from decouple import config

DEBUG: bool = config("DEBUG", cast=bool, default=False)
CACHE: bool = config("CACHE", cast=bool, default=True)
DATA_PATH: str = config("DATA_DIR", cast=str, default=os.path.join(os.path.dirname(__file__), "data"))
os.makedirs(DATA_PATH, exist_ok=True)
MODELS_PATH: str = config("MODELS_DIR", cast=str, default=os.path.join(DATA_PATH, "models"))
os.makedirs(MODELS_PATH, exist_ok=True)
CACHE_PATH: str = config("CACHE_DIR", cast=str, default=os.path.join(DATA_PATH, "cache"))
os.makedirs(CACHE_PATH, exist_ok=True)


class DB:
    NAME = config("DB_NAME", cast=str)
    USER = config("DB_USER", cast=str)
    PASS = config("DB_PASS", cast=str)
    HOST = config("DB_HOST", cast=str)
