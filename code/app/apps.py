import sys

from django.apps import AppConfig


class RecsConfig(AppConfig):
    name = 'app'

    def ready(self):
        if 'runserver' not in sys.argv:
            return True
        from app.blackbox.models import LanguageModel
        LanguageModel.create()
