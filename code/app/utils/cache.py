import os
import pickle
from typing import Callable, Any

from django.db.models import QuerySet

import config
from app.utils import Logger, Timer

CACHE_PATH = config.CACHE_PATH
ENABLED = config.CACHE

logger = Logger.get()


def get(key: str, generator: Callable[..., Any], *args, **kwargs) -> Any:
    os.makedirs(CACHE_PATH, exist_ok=True)

    file: str = ""
    if ENABLED:
        file = os.path.join(CACHE_PATH, "%s.p" % key)
        if os.path.exists(file):
            timer = Timer("found [%s] - loading" % key)
            result: Any = pickle.load(open(file, "rb"))
            timer.stop()
            return result

    timer = Timer("[%s] not found - generating and caching" % key)
    result: Any = generator(*args, *kwargs)
    if ENABLED:
        pickle.dump(result, open(file, "wb"))
    timer.stop()
    return result


def get_from_query(key: str, query: QuerySet) -> Any:
    return get(key, lambda: list(query))
