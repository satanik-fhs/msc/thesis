import os

import matplotlib.pyplot as plt
import scipy.stats
import numpy as np

import config


def plot_normal_distribution(mean: float, standard_deviation: float, filename: str, path: str = None, **kwargs):
    """

    :param mean:
    :param standard_deviation:
    :param filename:
    :param path:
    :param title: title of the plot
    :param subtitle: subtitle of the plot
    :return:
    """
    if not path:
        path = config.DATA_PATH
    os.makedirs(path, exist_ok=True)

    x_min = mean - 3.0 * standard_deviation
    x_max = mean + 3.0 * standard_deviation

    x = np.linspace(x_min, x_max, 100)

    y = scipy.stats.norm.pdf(x, mean, standard_deviation)

    plt.plot(x, y, color='black')

    # ----------------------------------------------------------------------------------------#
    # fill area 1

    pt1 = mean + standard_deviation
    plt.plot([pt1, pt1], [0.0, scipy.stats.norm.pdf(pt1, mean, standard_deviation)], color='black')

    pt2 = mean - standard_deviation
    plt.plot([pt2, pt2], [0.0, scipy.stats.norm.pdf(pt2, mean, standard_deviation)], color='black')

    ptx = np.linspace(pt1, pt2, 10)
    pty = scipy.stats.norm.pdf(ptx, mean, standard_deviation)

    plt.fill_between(ptx, pty, color='#0b559f', alpha='1.0')

    # ----------------------------------------------------------------------------------------#
    # fill area 2

    pt1 = mean + standard_deviation
    plt.plot([pt1, pt1], [0.0, scipy.stats.norm.pdf(pt1, mean, standard_deviation)], color='black')

    pt2 = mean + 2.0 * standard_deviation
    plt.plot([pt2, pt2], [0.0, scipy.stats.norm.pdf(pt2, mean, standard_deviation)], color='black')

    ptx = np.linspace(pt1, pt2, 10)
    pty = scipy.stats.norm.pdf(ptx, mean, standard_deviation)

    plt.fill_between(ptx, pty, color='#2b7bba', alpha='1.0')

    # ----------------------------------------------------------------------------------------#
    # fill area 3

    pt1 = mean - standard_deviation
    plt.plot([pt1, pt1], [0.0, scipy.stats.norm.pdf(pt1, mean, standard_deviation)], color='black')

    pt2 = mean - 2.0 * standard_deviation
    plt.plot([pt2, pt2], [0.0, scipy.stats.norm.pdf(pt2, mean, standard_deviation)], color='black')

    ptx = np.linspace(pt1, pt2, 10)
    pty = scipy.stats.norm.pdf(ptx, mean, standard_deviation)

    plt.fill_between(ptx, pty, color='#2b7bba', alpha='1.0')

    # ----------------------------------------------------------------------------------------#
    # fill area 4

    pt1 = mean + 2.0 * standard_deviation
    plt.plot([pt1, pt1], [0.0, scipy.stats.norm.pdf(pt1, mean, standard_deviation)], color='black')

    pt2 = mean + 3.0 * standard_deviation
    plt.plot([pt2, pt2], [0.0, scipy.stats.norm.pdf(pt2, mean, standard_deviation)], color='black')

    ptx = np.linspace(pt1, pt2, 10)
    pty = scipy.stats.norm.pdf(ptx, mean, standard_deviation)

    plt.fill_between(ptx, pty, color='#539ecd', alpha='1.0')

    # ----------------------------------------------------------------------------------------#
    # fill area 5

    pt1 = mean - 2.0 * standard_deviation
    plt.plot([pt1, pt1], [0.0, scipy.stats.norm.pdf(pt1, mean, standard_deviation)], color='black')

    pt2 = mean - 3.0 * standard_deviation
    plt.plot([pt2, pt2], [0.0, scipy.stats.norm.pdf(pt2, mean, standard_deviation)], color='black')

    ptx = np.linspace(pt1, pt2, 10)
    pty = scipy.stats.norm.pdf(ptx, mean, standard_deviation)

    plt.fill_between(ptx, pty, color='#539ecd', alpha='1.0')

    # ----------------------------------------------------------------------------------------#
    # fill area 6

    pt1 = mean + 3.0 * standard_deviation
    plt.plot([pt1, pt1], [0.0, scipy.stats.norm.pdf(pt1, mean, standard_deviation)], color='black')

    pt2 = mean + 10.0 * standard_deviation
    plt.plot([pt2, pt2], [0.0, scipy.stats.norm.pdf(pt2, mean, standard_deviation)], color='black')

    ptx = np.linspace(pt1, pt2, 10)
    pty = scipy.stats.norm.pdf(ptx, mean, standard_deviation)

    plt.fill_between(ptx, pty, color='#89bedc', alpha='1.0')

    # ----------------------------------------------------------------------------------------#
    # fill area 7

    pt1 = mean - 3.0 * standard_deviation
    plt.plot([pt1, pt1], [0.0, scipy.stats.norm.pdf(pt1, mean, standard_deviation)], color='black')

    pt2 = mean - 10.0 * standard_deviation
    plt.plot([pt2, pt2], [0.0, scipy.stats.norm.pdf(pt2, mean, standard_deviation)], color='black')

    ptx = np.linspace(pt1, pt2, 10)
    pty = scipy.stats.norm.pdf(ptx, mean, standard_deviation)

    plt.fill_between(ptx, pty, color='#89bedc', alpha='1.0')

    # ----------------------------------------------------------------------------------------#

    plt.grid()

    plt.xlim(0, 1.0)

    if "title" in kwargs:
        plt.suptitle(kwargs["subtitle"])
    if "subtitle" in kwargs:
        plt.title(kwargs["title"])

    plt.xlabel('x')
    plt.ylabel('Normal Distribution')

    plt.savefig(os.path.join(path, filename))
    plt.close()
