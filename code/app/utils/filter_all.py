from typing import Callable, Any, Iterable


def filter_all(predicate: Callable[[Any], bool], collection: Iterable):
    return all(map(predicate, collection))
