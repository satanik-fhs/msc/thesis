import sys
import inspect
import pkgutil
from pathlib import Path
from importlib import import_module
from typing import Type


def dynamic_import(base_cls: Type):
    stack_item = None
    for idx, item in enumerate(inspect.stack()):
        if getattr(item, 'function') == 'dynamic_import':
            stack_item = inspect.stack()[idx + 1]
            break
    current_module = inspect.getmodule(stack_item[0]).__name__
    for (_, name, _) in pkgutil.iter_modules([Path(stack_item.filename).parent]):

        imported_module = import_module('.' + name, package=current_module)

        for i in dir(imported_module):
            attribute = getattr(imported_module, i)

            if inspect.isclass(attribute) and issubclass(attribute, base_cls):
                setattr(sys.modules[current_module], name, attribute)
