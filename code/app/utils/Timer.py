import datetime

import time
from typing import Optional, Callable, Any

from app.utils import Logger

logger = Logger.get()


class Timer:
    def __init__(self, message: str = None, _stack=2):
        self.__stack = _stack
        self.time: float = time.time()
        self.message = message
        self.used: bool = False
        log: str = ""
        if self.message:
            log = "%s - " % self.message
        logger.debug("%stimer start" % log, stacklevel=self.__stack)

    def stop(self) -> float:
        if self.used:
            raise Exception("a timer can only be used once")
        total: float = time.time() - self.time
        readable: str = str(datetime.timedelta(seconds=total))
        log: str = ""
        if self.message:
            log = "%s - " % self.message
        logger.debug("%stook %s" % (log, readable), stacklevel=self.__stack)
        self.used = True
        return total


def timed(message: str = None) -> Callable[..., Any]:
    def parametrized(decorated: Callable[..., Any]):
        def decorating(*args, **kwargs) -> Any:
            args_str: str = str(args)
            kwargs_str: str = str(kwargs)
            log: str = "[%s] called with args [%s] and kwargs [%s]" % (
                decorated.__name__,
                args_str[:10] + '...' if len(args_str) > 10 else args_str,
                kwargs_str[:10] + '...' if len(kwargs_str) > 10 else kwargs_str
            )
            if message:
                log += ": %s" % message
            timer = Timer(log, _stack=3)
            result: Any = decorated(*args, **kwargs)
            timer.stop()
            return result
        return decorating
    return parametrized


class RepeatedTimer:
    def __init__(self, message: str = None, runs: Optional[int] = None):
        self.message: str = message
        self.used: bool = False
        self.runs: Optional[int] = runs
        self.timer: Optional[Timer] = None
        self.run: int = 0
        self.total: float = 0

        log: str = ""
        if self.message:
            log = "%s" % self.message

        logger.debug("%s - repeated timer start" % log, stacklevel=2)

    def start(self, message: str = None):
        if self.used:
            raise Exception("the repeated timer was already finished")

        if message:
            self.timer = Timer(message, _stack=3)
        else:
            self.timer = Timer(self.message, _stack=3)

    def stop(self):
        if self.used:
            raise Exception("the repeated timer was already finished")
        self.run += 1
        self.total += self.timer.stop()

        avg: float = self.total/self.run if self.run != 0 else 0
        log = "Stats:\n\n\tAVG:\t%s\n\tETD:\t%s" % (
            str(datetime.timedelta(seconds=avg)),
            str(datetime.timedelta(seconds=self.total))
        )

        if self.runs:
            log = "%s\n\tETA:\t%s" % (
                log,
                str(datetime.timedelta(seconds=(self.runs - self.run) * avg))
            )
        log = "%s\n" % log
        logger.debug(log, stacklevel=2)

    def finish(self) -> float:
        if self.used:
            raise Exception("the repeated timer was already finished")
        readable: str = str(datetime.timedelta(seconds=self.total))
        log: str = ""
        if self.message:
            log = "%s - " % self.message
        logger.debug("%srepeated timer took %s" % (log, readable), stacklevel=2)
        self.used = True
        return self.total
