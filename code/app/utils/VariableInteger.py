from typing import List


class VariableInteger(object):

    def __init__(self, bases=None):
        if bases is None:
            bases = [10]
        self.bases: List[int] = bases
        self.length: int = len(bases)
        self.max: List[int] = [b - 1 for b in bases]
        self.min: List[int] = [0] * self.length
        self.number: List[int] = self.min.copy()

    def is_min(self) -> bool:
        return self.number == self.min

    def is_max(self) -> bool:
        return self.number == self.max

    def __add__(self, other):
        n = VariableInteger(self.bases)
        n.number = self.number
        n += other
        return n

    def __iadd__(self, other):
        if not isinstance(other, int):
            return

        number: List[int] = self.number.copy()

        for i in range(other):
            if self.is_max():
                raise ArithmeticError('end of number reached: %s' % self)

            incremented: bool = False
            pointer: int = self.length - 1
            while not incremented:
                digit = number[pointer]
                if digit < self.max[pointer]:
                    number[pointer] += 1
                    incremented = True
                else:
                    number[pointer] = 0
                    pointer -= 1

        del self.number
        self.number = number
        return self

    def __sub__(self, other):
        n = VariableInteger(self.bases)
        n.number = self.number
        n -= other
        return n

    def __isub__(self, other):
        if not isinstance(other, int):
            return

        number: List[int] = self.number.copy()

        for i in range(other):
            if self.is_min():
                raise ArithmeticError('end of number reached: %s' % self)

            decremented: bool = False
            pointer: int = self.length - 1
            while not decremented:
                digit = number[pointer]
                if digit > 0:
                    number[pointer] -= 1
                    decremented = True
                else:
                    number[pointer] = self.max[pointer]
                    pointer -= 1

        del self.number
        self.number = number
        return self

    def __getitem__(self, pointer: int):
        if pointer < 0 or pointer > self.length:
            raise IndexError("pointer must point to a digit in the number inside [%d, %d]" % (0, self.length))
        return self.number[pointer]

    def __str__(self):
        return ' '.join(["%d_%d" % (self.number[i], self.max[i] + 1) for i in range(len(self.number))])
