from typing import Union, Tuple, Callable, Generator
import xml.etree.ElementTree as etree


def fast_iter(
        path: str,
        events: Union[str, Tuple[str]] = ('start', 'end', 'ns-start', 'ns-end'),
        tag: Union[str, Tuple[str]] = None,
        predicate: Callable[[str, etree.Element], bool] = None
) -> Generator[etree.Element, None, None]:
    context = etree.iterparse(path, events=events)
    context = iter(context)
    _, root = next(context)
    for event, elem in context:  # type: str, etree.Element
        should_yield = True
        if type(tag) is str and elem.tag != tag:
            should_yield = False
        elif type(tag) is tuple and elem.tag not in tag:
            should_yield = False
        elif not predicate(event, elem):
            should_yield = False

        if should_yield:
            yield elem

        if 'end' not in events or event == 'end':
            elem.clear()
        root.clear()
