from django.http import JsonResponse

from app.blackbox.blackbox import BlackBox
from app.models import Post
from app.utils import Logger

logger = Logger.get()


def index(request):
    post_a = Post.objects.order_by('id')[0]
    post_b = Post.objects.order_by('id')[1]
    result = BlackBox.calculate_similarity(post_a, post_b)

    return JsonResponse({'result': result})
