from __future__ import annotations

import functools
from collections import MutableSequence
from enum import Enum
from typing import Any, Callable, Dict, Generic, Iterable, Iterator, List, NewType, TypeVar, Union, overload

from django.db import connection, models

import app
from app.utils import lazy_default

T = TypeVar('T', bound='BaseModel')


class Models(MutableSequence, Generic[T]):

    def __init__(self, *args):
        """

        :param data: MutableSequence[T]
        """
        first = args[0]
        if isinstance(first, MutableSequence):
            self._data: MutableSequence[T] = first
        else:
            self._data: MutableSequence[T] = []

    def insert(self, index: int, object: T) -> None:
        self._data.insert(index, object)

    @overload
    def __setitem__(self, i: int, o: T) -> None:
        self._data[i] = o

    def __setitem__(self, s: slice, o: Iterable[T]) -> None:
        self._data[s] = o

    @overload
    def __delitem__(self, i: int) -> None:
        del self._data[i]

    def __delitem__(self, i: slice) -> None:
        del self._data[i]

    def __getitem__(self, s) -> Union[Models[T], T]:
        if isinstance(s, slice):
            return Models(self._data[s])
        else:
            return self._data[s]

    def __len__(self) -> int:
        return len(self._data)

    def __iter__(self) -> Iterator[T]:
        return super().__iter__()

    def size(self) -> int:
        return len(self)

    def ids(self) -> Iterable[int]:
        return map(lambda m: m.id, self._data)

    def filter(self, predicate: Callable[[T], bool]) -> app.models.Models[T]:
        return Models(list(filter(predicate, self._data)))

    def map(self, predicate: Callable[[T], Any]) -> Iterable[Any]:
        return map(predicate, self._data)


class BaseModel(models.Model, Generic[T]):
    class Meta:
        abstract = True
    _cache: Dict[str, Models[T]] = {}
    @classmethod
    def cache(cls) -> Models[T]:
        name = cls.__name__
        if name not in BaseModel._cache:
            BaseModel._cache[name] = Models(list(cls.objects.all()))
        return BaseModel._cache[name]

    @classmethod
    def custom(cls, sql: str, params: MutableSequence[Any] = None) -> Models[T]:
        if not params:
            params = []

        with connection.cursor() as cursor:
            cursor.execute(sql, params)
            ids = list(map(lambda t: t[0], cursor.fetchall()))

        return Models(list(cls.objects.filter(id__in=ids).all()))


class User(BaseModel['User']):
    class Meta:
        db_table = "recs_user"
    id = models.AutoField(primary_key=True)
    reputation = models.PositiveIntegerField()
    creation_date = models.DateTimeField()
    display_name = models.CharField(max_length=200)
    last_access_date = models.DateTimeField()
    website_url = models.URLField(max_length=255)
    location = models.TextField()
    age = models.PositiveIntegerField(null=True)
    about_me = models.TextField()
    views = models.PositiveIntegerField()
    up_votes = models.PositiveIntegerField()
    down_votes = models.PositiveIntegerField()
    account_id = models.IntegerField(null=True)
    profile_image_url = models.URLField(null=True)

    _questions: Dict[int, List[int]] = {}
    _answers: Dict[int, List[int]] = {}

    @classmethod
    def questions_by_id(cls, id: int):
        return lazy_default(
            User._questions,
            id,
            lambda: list(Post.cache().filter(lambda p: p.user_id == id and p.post_type_id == Post.Type.QUESTION.value).ids()))

    @classmethod
    def questions_by_ids(cls, ids: List[int]):
        return functools.reduce(lambda accumulator, id: accumulator + User.questions_by_id(id), ids, [])

    def questions(self):
        return User.questions_by_id(self.id)

    @classmethod
    def answers_by_id(cls, id: int):
        return lazy_default(
            User._answers,
            id,
            lambda: list(Post.cache().filter(lambda p: p.user_id == id and p.post_type_id == Post.Type.ANSWER.value).ids()))

    @classmethod
    def answers_by_ids(cls, ids: List[int]):
        return functools.reduce(lambda accumulator, id: accumulator + User.answers_by_id(id), ids, [])

    def answers(self):
        return User.answers_by_id(self.id)


class Post(BaseModel['Post']):
    class Meta:
        db_table = "recs_post"

    class Type(Enum):
        QUESTION = '1'
        ANSWER = '2'

    user = models.ForeignKey(
        User,
        related_name='posts',
        on_delete=models.DO_NOTHING,
        db_column='owner_user_id'
    )
    parent = models.ForeignKey(
        "self",
        related_name='children',
        on_delete=models.DO_NOTHING,
        db_column='parent_id'
    )
    accepted_answer = models.ForeignKey(
        "self",
        related_name='question',
        on_delete=models.DO_NOTHING,
        db_column='accepted_answer_id'
    )

    id = models.AutoField(primary_key=True)
    post_type_id = models.CharField(
        max_length=8,
        choices=[(tag, tag.value) for tag in Type]
    )
    creation_date = models.DateTimeField()
    score = models.IntegerField()
    view_count = models.PositiveIntegerField(null=True)
    body = models.TextField()
    last_editor_user_id = models.IntegerField(null=True)
    last_editor_display_name = models.TextField(null=True)
    last_edit_date = models.DateTimeField(null=True)
    last_activity_date = models.DateTimeField(null=True)
    community_owned_date = models.DateTimeField(null=True)
    closed_date = models.DateTimeField(null=True)
    title = models.TextField()
    tags = models.TextField()
    answer_count = models.PositiveIntegerField(null=True)
    comment_count = models.PositiveIntegerField(null=True)
    favorite_count = models.PositiveIntegerField(null=True)


Question = NewType('Question', Post)
Answer = NewType('Answer', Post)


class PostHistory(models.Model):
    class Meta:
        db_table = "recs_posthistory"

    class TypeId(Enum):
        INITIAL_TITLE = 1
        INITIAL_BODY = 2
        INITIAL_TAGS = 3
        EDIT_TITLE = 4
        EDIT_BODY = 5
        EDIT_TAGS = 6
        ROLLBACK_TITLE = 7
        ROLLBACK_BODY = 8
        ROLLBACK_TAGS = 9
        POST_CLOSED = 10
        POST_REOPENED = 11
        POST_DELETED = 12
        POST_UNDELETED = 13
        POST_LOCKED = 14
        POST_UNLOCKED = 15
        COMMUNITY_OWNED = 16
        POST_MIGRATED = 17
        QUESTION_MERGED = 18
        QUESTION_PROTECTED = 19
        QUESTION_UNPROTECTED = 20
        POST_DISASSOCIATED = 21
        QUESTION_UNMERGED = 22

    class CloseReasonId(Enum):
        EXACT_DUPLICATE = 1
        OFF_TOPIC = 2
        SUBJECTIVE = 3
        NOT_A_REAL_QUESTION = 4
        TOO_LOCALIZED = 7

    id = models.AutoField(primary_key=True)
    post_history_type_id = models.SmallIntegerField(choices=[(tag.value, tag) for tag in TypeId])
    post_id = models.IntegerField(null=True)
    revision_guid = models.CharField(max_length=36, null=True)
    creation_date = models.DateTimeField()
    user_id = models.IntegerField(null=True)
    user_display_name = models.TextField(null=True)
    comment = models.TextField(null=True)
    text = models.TextField()
    close_reason_id = models.SmallIntegerField(choices=[(tag.value, tag) for tag in CloseReasonId], null=True)


class Comment(models.Model):
    class Meta:
        db_table = "recs_comment"

    id = models.AutoField(primary_key=True)
    post_id = models.IntegerField(null=True)
    score = models.IntegerField()
    text = models.TextField()
    creation_date = models.DateTimeField()
    user_id = models.IntegerField(null=True)


class Vote(models.Model):
    class Meta:
        db_table = "recs_vote"

    class TypeId(Enum):
        ACCEPTED_BY_ORIGINATOR = 1
        UP_MOD = 2
        DOWN_MOD = 3
        OFFENSIVE = 4
        FAVORITE = 5
        CLOSE = 6
        REOPEN = 7
        BOUNTY_START = 8
        BOUNTY_CLOSE = 9
        DELETION = 10
        UNDELETION = 11
        SPAM = 12
        INFORM_MODERATOR = 13

    id = models.AutoField(primary_key=True)
    post_id = models.IntegerField()
    vote_type_id = models.SmallIntegerField(choices=[(tag.value, tag) for tag in TypeId])
    creation_date = models.DateTimeField()
    user_id = models.IntegerField(null=True)
    bounty_amount = models.IntegerField(null=True)


class Badge(models.Model):
    class Meta:
        db_table = "recs_badge"

    id = models.AutoField(primary_key=True)
    name = models.TextField()
    user_id = models.IntegerField(null=True)
    date = models.DateTimeField()
    _class = models.SmallIntegerField(null=True, db_column='class')
    tag_based = models.BooleanField(null=True)


class Tag(models.Model):
    class Meta:
        db_table = "recs_tag"

    id = models.AutoField(primary_key=True)
    tag_name = models.TextField()
    count = models.IntegerField()
    excerpt_post_id = models.IntegerField(null=True)
    wiki_post_id = models.IntegerField(null=True)


class PostLink(models.Model):
    class Meta:
        db_table = "recs_postlink"

    class TypeId(Enum):
        LINKED = 1
        DUPLICATE = 3

    id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField()
    post_id = models.IntegerField()
    related_post_id = models.IntegerField()
    link_type_id = models.SmallIntegerField(choices=[(tag.value, tag) for tag in TypeId])
