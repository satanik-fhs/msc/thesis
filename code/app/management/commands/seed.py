import datetime
import linecache
import os
import tracemalloc
from importlib import import_module
from typing import Type

import gc
from django.conf import settings
from django.core.exceptions import FieldDoesNotExist
from django.core.management.base import BaseCommand
from django.db.models import Model
from django.utils.timezone import make_aware
from inflection import camelize, pluralize, singularize, underscore

from app.utils import BulkCreator, fast_iter
from app.utils import Logger


def display_top(snapshot: tracemalloc.Snapshot, key_type='lineno', limit=10) -> str:
    result: str = ""

    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    result += "Top %s lines" % limit + os.linesep
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        result += "#%s: %s:%s: %.1f KiB" % (index, filename, frame.lineno, stat.size / 1024) + os.linesep
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            result += '    %s' % line + os.linesep

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        result += "%s other: %.1f KiB" % (len(other), size / 1024) + os.linesep
    total = sum(stat.size for stat in top_stats)
    result += "Total allocated size: %.1f KiB" % (total / 1024) + os.linesep
    return result


class TakeSnapshot:
    counter = 0

    @staticmethod
    def run():
        filename = ("/tmp/tracemalloc-%d-%04d.dump"
                    % (os.getpid(), TakeSnapshot.counter))
        print("Write snapshot into %s..." % filename)
        gc.collect()
        snapshot = tracemalloc.take_snapshot()
        with open(filename, "w") as fp:
            # Pickle version 2 can be read by Python 2 and Python 3
            fp.write(display_top(snapshot))
        del snapshot
        print("Snapshot written into %s" % filename)
        TakeSnapshot.counter += 1


# Get an instance of a logger
logger = Logger.get()

# python manage.py seed <class>


class Command(BaseCommand):
    help = "seed database for testing and development."

    def __init__(self, stdout=None, stderr=None, no_color=False, force_color=False):
        super().__init__(stdout, stderr, no_color, force_color)
        self.chunk_size = None
        self.cls = None
        self.start = None

    def add_arguments(self, parser):
        parser.add_argument('class')
        parser.add_argument('--chunk-size', type=int, default=100)
        parser.add_argument('--start', type=int, default=-1000)

    def handle(self, *args, **options):
        logger.info('seeding data...')

        (
            self.cls,
            self.chunk_size,
            self.start
        ) = [options[k] for k in ('class', 'chunk_size', 'start')]

        self.seed_db()

        logger.info('done')

    def seed_db(self):
        # tracemalloc.start(25)
        # TakeSnapshot.run()

        logger.info("Creating 'recs.models.{}'s".format(singularize(camelize(self.cls))))

        try:
            cls_def: Type[Model] = getattr(import_module("recs.models"), singularize(camelize(self.cls)))
        except (ImportError, AttributeError) as e:
            raise ImportError(self.cls)

        creator = BulkCreator(cls_def, self.chunk_size)
        path = os.path.join(settings.BASE_DIR, "recs", "seeds", "%s.xml" % pluralize(camelize(self.cls)))

        # i = 0
        for elem in fast_iter(
                path,
                events=('start',),
                tag='row',
                predicate=lambda ev, el: int(el.attrib['Id']) >= self.start
        ):
            # if i % 100 == 0:
            #     TakeSnapshot.run()
            #     i += 1

            obj = cls_def()
            for k, v in elem.attrib.items():
                try:
                    # noinspection PyProtectedMember
                    field = cls_def._meta.get_field(underscore(k))
                    if field.get_internal_type() == "DateTimeField":
                        v = datetime.datetime.strptime(v, "%Y-%m-%dT%H:%M:%S.%f")
                        v = make_aware(v)
                    key = underscore(k)
                    if hasattr(obj, key):
                        setattr(obj, key, v)
                    elif hasattr(obj, "_%s" % key):
                        setattr(obj, "_%s" % key, v)
                except FieldDoesNotExist:
                    pass

            creator.append(obj)
        creator.create()

        logger.info("Created entries of type 'recs.models.{}'.".format(singularize(camelize(self.cls))))
