import csv
import inspect
import os
import statistics
from collections import OrderedDict
from typing import Dict, List, Optional, Set, Tuple, Type

import matplotlib.pyplot as plt
from django.core.management import BaseCommand
from django.db.models import Model
from inflection import dasherize, titleize, underscore

import config
from app.blackbox.models import BaseModel, ItemModel, LanguageModel, UserModel
from app.blackbox.recommender import ConfusionMatrix, QuestionsAnsweredBySimilarUsers, QuestionsBySimilarUsers, \
    UserModelRecommender, SimilarQuestions
from app.models import Answer, Models, Post, Question, User
from app.utils import Logger, RepeatedTimer, Timer, cache, lazy_default, plot_normal_distribution
from app.utils.progress import progress

logger = Logger.get()


class Command(BaseCommand):
    def __init__(self, stdout=None, stderr=None, no_color=False, force_color=False):
        super().__init__(stdout, stderr, no_color, force_color)
        self.self_test: Optional[bool] = None

    def add_arguments(self, parser):
        parser.add_argument('--self-test', type=str, default=False)

    def handle(self, *args, **options):
        timer = Timer('training data...')

        (
            self.self_test,
        ) = [options[k] for k in ('self_test',)]

        self._test()

        timer.stop()

    def _test(self):
        # LanguageModel.create()

        data_path = config.DATA_PATH
        models_path = config.MODELS_PATH
        results_path = os.path.join(data_path, "results")
        os.makedirs(results_path, exist_ok=True)
        images_path = os.path.join(data_path, "images")
        os.makedirs(images_path, exist_ok=True)

        text_models = [
            # "answers_doc2vec_pv_dbow",
            # "answers_doc2vec_pv_dm",
            # "questions_doc2vec_pv_dbow",
            # "questions_doc2vec_pv_dm",
            # "posts_doc2vec_pv_dbow",
            # "posts_doc2vec_pv_dm",
            "answers_tfidf_ntc",
            "questions_tfidf_ntc",
            # "answers_lda_standard",
            # "questions_lda_standard"
        ]

        users: Models[User] = cache.get("users", lambda: User.cache())
        User._cache[User.__name__] = users
        posts: Models[Post] = cache.get("posts", lambda: Post.cache())
        Post._cache[Post.__name__] = posts
        questions: Models[Question] = cache.get("questions", lambda: posts.filter(lambda p: p.post_type_id == Post.Type.QUESTION.value))
        answers: Models[Answer] = cache.get("answers", lambda: posts.filter(lambda p: p.post_type_id == Post.Type.ANSWER.value))
        user_answers: Dict[int, Set[int]] = {}

        def self_similarity(model: BaseModel, objects: List[Model]) -> Dict[int, float]:
            result: Dict[int, float] = {}
            size = len(objects)
            for i, obj in enumerate(objects):
                timer = Timer("self similarity [%d of %d (%d%%)]" % (i, size, int(i/size*100)))
                result[obj.id] = model.similarity(obj, obj)
                timer.stop()
            return result

        def self_ranking(model: BaseModel, objects: List[Model]) -> Dict[int, int]:
            result: Dict[int, int] = {}
            size = len(objects)
            for i, obj in enumerate(objects):
                timer = Timer("self ranking [%d of %d (%d%%)]" % (i, size, int(i / size * 100)))
                sims: OrderedDict[int, float] = OrderedDict(model.similarities(obj, size))
                try:
                    result[obj.id] = list(sims.keys()).index(obj.id)
                except ValueError as e:
                    result[obj.id] = -1
                timer.stop()
            return result

        if self.self_test:
            models: List[Tuple[Type[BaseModel], Models[Model]]] = [(ItemModel, posts), (UserModel, users)]

            for t in models:
                (model, data) = t
                cls_name = underscore(model.__name__.replace("Model", ""))
                for name in text_models:
                    try:
                        instance = model.load(name, models_path)
                    except FileNotFoundError as e:
                        continue
                    # self_similarities: Dict[int, float] = cache.get(
                    #     "%s.%s.self_similarities" % (name, cls_name),
                    #     self_similarity,
                    #     instance,
                    #     data
                    # )
                    self_rankings: Dict[int, int] = cache.get(
                        "%s.%s.self_rankings" % (name, cls_name),
                        self_ranking,
                        instance,
                        data
                    )

                    accuracy = 0
                    for id, ranking in self_rankings.items():
                        if ranking == 0:
                            accuracy += 1
                    accuracy /= len(self_rankings)

                    file = os.path.join(images_path, "%s.%s.self_rankings.png" % (name, cls_name))

                    plt.hist(self_rankings.values())
                    plt.title("How many rank best against themselves")
                    plt.suptitle("Accuracy: %f" % accuracy)
                    plt.savefig(file)
                    plt.close()

        users_length: int = users.size()
        questions_length: int = questions.size()

        def create_confusion_matrix(recommender: UserModelRecommender, users: Models[User]) -> Dict[int, List[Dict[str, float]]]:
            cm: Dict[int, List[Dict[str, float]]] = {}
            rec_name: str = underscore(recommender.__class__.__name__)
            text_model: str = recommender._filename
            repeated_timer = RepeatedTimer("CM (%s.%s)" % (rec_name, text_model), runs=users_length)
            for n, user in enumerate(users):

                repeated_timer.start("%s [%d of %d]" % (
                    progress(n, users_length, prefix='processing user:', suffix='', length=50),
                    n, users_length))

                def find_answer_ids() -> Set[int]:
                    filtered: Models[Answer] = answers.filter(lambda a: user.id == a.user_id and a.post_type_id == Post.Type.ANSWER.value)
                    return set(filtered.ids())

                answered_ids: Set[int] = lazy_default(user_answers, user.id, find_answer_ids)

                timer = Timer("recommendation")
                recommendations: List[int] = recommender.recommend(user, 10)
                timer.stop()

                for k in range(10, 11, 10):
                    if k not in cm:
                        cm[k] = []

                    subset: List[int] = recommendations[:k]

                    rec_ids: Set[int] = set(subset)
                    correct_recommendations: Set[int] = rec_ids.intersection(answered_ids)
                    wrong_recommendations: Set[int] = rec_ids.difference(answered_ids)
                    missing_recommendations: Set[int] = answered_ids.difference(rec_ids)
                    # non_recommendations: all others

                    tp = len(correct_recommendations)
                    fp = len(wrong_recommendations)
                    fn = len(missing_recommendations)
                    tn = questions_length - (tp + fp + fn)

                    c = ConfusionMatrix(TP=tp, FP=fp, FN=fn, TN=tn)
                    measures = {}
                    for key, _ in inspect.getmembers(ConfusionMatrix, predicate=lambda m: inspect.isfunction(m) and m.__name__ != "__init__"):
                        measures[dasherize(key)] = getattr(c, key)()
                    cm[k].append(measures)
                repeated_timer.stop()
            repeated_timer.finish()
            return cm

        recommenders = [
            QuestionsAnsweredBySimilarUsers,
            # SimilarQuestions,
            # QuestionsBySimilarUsers,
        ]
        for recommender in recommenders:
            for name in text_models:
                rec = recommender(name, models_path)
                try:
                    confusion_matrix: Dict[int, List[Dict[str, float]]] = cache.get(
                        "%s.%s.stats" % (underscore(rec.__class__.__name__), name),
                        create_confusion_matrix,
                        rec,
                        users
                    )
                except FileNotFoundError as e:
                    logger.debug(str(e))
                    continue

                if len(confusion_matrix) == 0:
                    continue

                base_path: str = os.path.join(
                    results_path,
                    "%s.%s" % (underscore(recommender.__name__), underscore(name))
                )

                os.makedirs(base_path, exist_ok=True)

                csv_path: str = os.path.join(base_path, "confusion_matrix_stats.csv")
                csv_file = open(csv_path, mode='w')
                fields_names = ["measure", "min", "max", "mean", "median","standard deviation", "variance"]
                writer = csv.DictWriter(csv_file, fieldnames=fields_names)
                writer.writeheader()
                
                results_image_path: str = os.path.join(base_path, "images")
                os.makedirs(results_image_path, exist_ok=True)

                for top_n, cm_list in confusion_matrix.items():
                    for key, _ in inspect.getmembers(ConfusionMatrix, predicate=lambda m: inspect.isfunction(m) and m.__name__ != "__init__"):
                        if dasherize(key) in cm_list[0]:

                            file: str = os.path.join(results_image_path, "%d_%s.png" % (top_n, underscore(key)))

                            values: List[float] = list(map(lambda cm: cm[dasherize(key)], cm_list))
                            stat = {
                                "measure": titleize(key),
                                "min": min(values),
                                "max": max(values),
                                "mean": statistics.mean(values),
                                "median": statistics.median(values),
                                "standard deviation": statistics.stdev(values),
                                "variance": statistics.variance(values)
                            }
                            writer.writerow(stat)

                            plot_normal_distribution(
                                stat["mean"],
                                stat["standard deviation"],
                                "%d_%s.stats.png" % (top_n, underscore(key)),
                                results_image_path,
                                title="%s (%d) - %s" % (titleize(recommender.__name__), top_n, name),
                                subtitle="%s" % titleize(key)
                            )

                            plt.hist(values)
                            plt.title("%s (%d) - %s" % (titleize(recommender.__name__), top_n, name))
                            plt.suptitle(titleize(key))
                            plt.xlabel(titleize(key))
                            plt.ylabel("Number of values")
                            plt.savefig(file)
                            plt.close()
                csv_file.close()
                # tpr = list(map(lambda e: e["recall"], cm))
                # fpr = list(map(lambda e: e["false-positive-rate"], cm))
                # roc_auc = metrics.auc(fpr, tpr)
                # plt.title('Receiver Operating Characteristic')
                # plt.plot(fpr, tpr, 'b', label='AUC = %0.2f' % roc_auc)
                # plt.legend(loc='lower right')
                # plt.plot([0, 1], [0, 1], 'r--')
                # plt.xlim([0, 1])
                # plt.ylim([0, 1])
                # plt.ylabel('True Positive Rate')
                # plt.xlabel('False Positive Rate')
                # plt.show()
                # plt.savefig("data/images/%s.%s.cm.png" % (underscore(rec.__class__.__name__), name))

