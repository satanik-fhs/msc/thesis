class ConfusionMatrix:
    def __init__(self, *args, **kwargs):
        """

        :param TP: true positives - correctly selected
        :param TN: true negatives - correctly discarded
        :param FP: false positives - wrongly selected
        :param FN: false negatives - should have been selected
        """
        keys = ["TP", "TN", "FP", "FN", "size"]
        self.TP, self.TN, self.FP, self.FN, self.size = None, None, None, None, None

        for i, arg in enumerate(args):
            setattr(self, keys[i], arg)

        for key, arg in kwargs.items():
            if key in keys:
                setattr(self, key, arg)

        if self.size is None:
            for key in keys[:-1]:
                if getattr(self, key) is None:
                    raise ValueError("if size is not set at least [%s] has to be set" % keys[:-1])
                self.size = sum(getattr(self, key) for key in keys[:-1])
        else:
            none_key = None
            for key in keys[:-1]:
                if getattr(self, key) is None:
                    if none_key:
                        raise ValueError("only one of [%s] may be None" % key[:-1])
                    none_key = key
                    break
            others = sum(getattr(self, key) for key in keys[:-1] if key != none_key)
            setattr(self, none_key, self.size - others)

    def precision(self) -> float:
        # true positive rate
        if self.TP + self.FP == 0:
            return 1.0
        return self.TP / (self.TP + self.FP)

    def recall(self) -> float:
        if self.TP + self.FN == 0:
            return 1.0
        return self.TP / (self.TP + self.FN)

    # def true_negative_rate(self) -> float:
    #     # specificity
    #     if self.TN + self.FP == 0:
    #         return 1.0
    #     return self.TN / (self.TN + self.FP)

    # def false_positive_rate(self) -> float:
    #     return 1 - self.true_negative_rate()

    def accuracy(self) -> float:
        return (self.TP + self.TN) / (self.TP + self.TN + self.FP + self.FN)

    # def balanced_accuracy(self) -> float:
    #     return (self.recall() + self.true_negative_rate()) / 2

    def f_measure(self) -> float:
        return 2 * (self.precision() * self.recall())
