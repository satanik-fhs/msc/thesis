from typing import List

from django.db.models import Model

from app.blackbox.recommender import Recommender


class AnsweredSameQuestionRecommender(Recommender):
    def recommend(self, a: Model, top_n: int = 10) -> List[int]:
        pass