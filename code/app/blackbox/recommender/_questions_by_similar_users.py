from typing import Iterable, List, Tuple

from app.blackbox.recommender import UserModelRecommender
from app.models import Post, Question, User


class QuestionsBySimilarUsers(UserModelRecommender):

    def recommend(self, a: User, top_n: int = 10) -> List[int]:
        user_model = self._get_user_model()
        users: List[Tuple[int, float]] = user_model.similarities(a, top_n=top_n)
        user_ids: Iterable[int] = list(map(lambda t: t[0], users))
        questions: List[Question] = Post.objects.filter(user__id__in=user_ids, post_type_id=int(Post.Type.QUESTION.value))
        return list(map(lambda q: q.id, questions))
