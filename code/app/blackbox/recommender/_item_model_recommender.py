import abc
from typing import List

import config
from app.blackbox.models import ItemModel
from app.blackbox.recommender import Recommender
from app.models import User


class ItemModelRecommender(Recommender, abc.ABC):
    def __init__(self, filename: str, path: str = config.DATA_PATH):
        """

        :param filename: base name of item model without item.model extension
        :param path: path to the item model file
        """
        self._filename = filename
        self._path = path

    def _get_item_model(self) -> ItemModel:
        return ItemModel.load(self._filename, self._path)

    @abc.abstractmethod
    def recommend(self, a: User, top_n: int = 10) -> List[int]:
        pass
