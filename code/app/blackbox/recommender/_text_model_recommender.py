import abc

import config
from app.blackbox.models import TextModel
from app.blackbox.recommender import Recommender


class TextModelRecommender(Recommender, abc.ABC):
    def __init__(self, filename: str, path: str = config.DATA_PATH):
        """

        :param filename: base name of user model without user.model extension
        :param path: path to the user model file
        """
        self._filename = filename
        self._path = path

    def _get_item_model(self) -> TextModel:
        return TextModel.load(self._filename, self._path)
