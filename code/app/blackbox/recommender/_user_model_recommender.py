import abc
from typing import List

import config
from app.blackbox.models import UserModel
from app.blackbox.recommender import Recommender
from app.models import User


class UserModelRecommender(Recommender, abc.ABC):
    def __init__(self, filename: str, path: str = config.DATA_PATH):
        """

        :param filename: base name of user model without user.model extension
        :param path: path to the user model file
        """
        self._filename = filename
        self._path = path

    def _get_user_model(self) -> UserModel:
        return UserModel.load(self._filename, self._path)

    @abc.abstractmethod
    def recommend(self, a: User, top_n: int = 10) -> List[int]:
        pass
