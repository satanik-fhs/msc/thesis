from typing import Iterable, List, Tuple

from django.db.models import Model

from app.blackbox.models import ItemModel
from app.blackbox.recommender import ItemModelRecommender


class QuestionsRecommender(ItemModelRecommender):
    def recommend(self, a: Model, top_n: int = 10) -> List[int]:
        item_model: ItemModel = self._get_item_model()
        posts: List[Tuple[int, float]] = item_model.similarities(a, top_n=top_n)
        ids: Iterable[int] = map(lambda t: t[0], posts)
        return list(ids)
