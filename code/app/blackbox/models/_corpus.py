from typing import List, Iterable, Union, NewType

from gensim.models.doc2vec import TaggedDocument
from spacy.tokens import Doc

from app.blackbox.models._language_model import LanguageModel
from app.utils import Logger
from app.utils.progress import progress

logger = Logger.get()
Words = NewType("words", List[str])


class Corpus(object):
    def __init__(self, entities: Iterable, field_name: str, tokens_only: bool = False):
        self.entities = entities
        self.field_name = field_name
        self.tokens_only = tokens_only

    def __iter__(self) -> Union[Words, TaggedDocument]:
        length = sum(1 for _ in self.entities)
        LanguageModel.create()
        print(progress(0, length, 'Process document'), end="")
        for i, entity in enumerate(self.entities):
            try:
                text: str = getattr(entity, self.field_name)
            except AttributeError as e:
                text: str = entity[self.field_name]
            if len(text) > 100000:
                continue
            words: List[str] = Corpus.lemmatize(text)
            if self.tokens_only:
                yield words
            else:
                try:
                    id: int = entity.id
                except AttributeError as e:
                    id: int = entity['id']
                yield TaggedDocument(words=words, tags=[id])
            print(progress(i+1, length, 'Process document', "[%d of %d]" % (i, length)), end="")
        print()

    @classmethod
    def lemmatize(cls, text: str) -> List[str]:
        doc: Doc = LanguageModel.preprocess_text(text)
        words: List[str] = [t.lemma_ for t in doc]
        return words
