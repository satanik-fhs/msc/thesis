from __future__ import annotations

from copy import deepcopy
from typing import Any, Dict, List, Tuple, Type

from django.db.models import Model
from inflection import underscore

from app.blackbox.models import BaseModel, Corpus, TextModel
from app.utils import Timer


class ItemModel(BaseModel):

    def __init__(self, model_cls: Type[Model], field: str, match: Dict[str, Any], model_type: Type[TextModel],
                 method: TextModel.Method):
        super().__init__(model_type, method)
        self.__model_cls: Type[Model] = model_cls
        self.__field: str = field
        self.__match: Dict[str, Any] = deepcopy(match)

    def train(self):
        data = self.__model_cls.objects.all()
        for key, val in self.__match.items():
            data = data.filter(**{underscore(key): val})

        timer = Timer("gathering data for item model generation")
        fetched = list(data)
        timer.stop()

        timer = Timer("creating corpus for item model generation")
        corpus = Corpus(fetched, underscore(self.__field), False)
        timer.stop()

        self._model.train(corpus)

    def similarities(self, item: Model, top_n: int) -> List[Tuple[int, float]]:
        words: List[str] = Corpus.lemmatize(getattr(item, underscore(self.__field)))
        similarities = self._model.similarities(words, top_n)
        return similarities

    def similarity(self, a: Model, b: Model) -> float:
        similarity = self._model.similarity(
            Corpus.lemmatize(getattr(a, underscore(self.__field))),
            Corpus.lemmatize(getattr(b, underscore(self.__field)))
        )
        return similarity
