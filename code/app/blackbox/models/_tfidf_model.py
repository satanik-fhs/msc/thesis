import glob
import os
import pickle
from typing import List, Tuple, Optional, Union

from gensim import corpora, similarities, matutils
from gensim.corpora import MmCorpus
from gensim.models import TfidfModel as gensim_tfidf_model
from gensim.models.doc2vec import TaggedDocument

import config
from app.blackbox.models._corpus import Corpus, Words
from app.blackbox.models._text_model import TextModel
from app.utils import Logger, Timer, cache
from app.utils.Timer import timed

logger = Logger.get()


class TfidfModel(TextModel):

    class Method(TextModel.Method):
        NTC = "NTC"

    SETTINGS = {
        "NTC": "ntc"
    }

    def __init__(self, method: Method):
        super().__init__(method)
        logger.info("creating TF-IDF model with method [%s]" % method.value)
        self._setting: str = TfidfModel.SETTINGS[method.value]
        self._dictionary: Optional[corpora.Dictionary] = None
        self._model: Optional[gensim_tfidf_model] = None
        self._similarity_model: Optional[similarities.MatrixSimilarity] = None
        self._index2id: List[int] = []

    def train(self, corpus: Corpus):
        timer = Timer("training TF-IDF model")

        @timed("processing corpus")
        def process(corpus: Corpus) -> List[TaggedDocument]:
            return list(corpus)

        processed = cache.get('tfidf.corpus', process, corpus)

        @timed("creating index2id list")
        def create_index2_id(processed: List[TaggedDocument]) -> List[str]:
            return list(map(lambda t: t.tags[0], processed))

        self._index2id = cache.get('tfidf.index2id', create_index2_id, processed)

        @timed("create documents")
        def create_documents(processed: List[TaggedDocument]) -> List[str]:
            return list(map(lambda t: t.words, processed))

        documents: List[str] = cache.get('tfidf.documents', create_documents, processed)

        @timed("create dictionary")
        def create_dictionary(documents: List[str]) -> corpora.Dictionary:
            return corpora.Dictionary(documents)

        self._dictionary = cache.get('tfidf.dictionary', create_dictionary, documents)

        @timed("create bow corpus")
        def create_bow_corpus(documents: List[str]) -> List[List[Tuple[int, int]]]:
            return list(map(lambda t: self._dictionary.doc2bow(t), documents))

        corpus_bow = cache.get('tfidf.corpus_bow', create_bow_corpus, documents)

        @timed("create vectorized corpus")
        def create_vectorized_corpus(corpus_bow: List[List[Tuple[int, int]]]) -> MmCorpus:
            path = os.path.join(config.CACHE_PATH, "tfidf.vectorized_corpus.mm")
            MmCorpus.serialize(path, corpus_bow)
            return MmCorpus(path)

        vectorized_corpus = cache.get('tfidf.vectorized_corpus', create_vectorized_corpus, corpus_bow)

        @timed("create tfidf model")
        def create_tfidf_model(corpus_bow: List[List[Tuple[int, int]]]) -> gensim_tfidf_model:
            return gensim_tfidf_model(corpus_bow, smartirs=self._setting)

        self._model = cache.get('tfidf.model', create_tfidf_model, corpus_bow)

        @timed("create similarity matrix")
        def create_similarity_matrix(tfidf: gensim_tfidf_model, vectorized_corpus: MmCorpus) -> similarities.SparseMatrixSimilarity:
            return similarities.SparseMatrixSimilarity(tfidf[vectorized_corpus], num_features=len(self._dictionary))

        self._similarity_model = cache.get('tfidf.similarities', create_similarity_matrix, self._model, vectorized_corpus)
        timer.stop()

    def save(self, filename: str, path: str = None):
        if not path:
            path = config.DATA_PATH
        else:
            os.makedirs(path, mode=0o755, exist_ok=True)
        file_path = "%s/%s.tfidf.model" % (path, filename)
        timer = Timer("saving TF-IDF model to [%s]" % file_path)
        self._model.save(file_path)
        file_path = "%s/%s.tfidf.index2id.model" % (path, filename)
        pickle.dump(self._index2id, open(file_path, mode="wb"))
        file_path = "%s/%s.tfidf.dictionary.model" % (path, filename)
        self._dictionary.save(file_path)
        file_path = "%s/%s.tfidf.similarity.model" % (path, filename)
        self._similarity_model.save(file_path)

        file_paths = glob.glob(os.path.join(config.CACHE_PATH, 'tfidf.*'))
        for file_path in file_paths:
            try:
                os.remove(file_path)
            except NotImplementedError as e:
                logger.error(e)
        timer.stop()

    @classmethod
    def load(cls, filename: str, path: str = None):
        if not path:
            path = config.DATA_PATH
        file_path = "%s/%s.tfidf.model" % (path, filename)
        timer = Timer("loading TF-IDF model from [%s]" % file_path)
        model = TfidfModel.__new__(TfidfModel)
        model._model = gensim_tfidf_model.load(file_path)
        file_path = "%s/%s.tfidf.index2id.model" % (path, filename)
        model._index2id = pickle.load(open(file_path, mode="rb"))
        file_path = "%s/%s.tfidf.dictionary.model" % (path, filename)
        model._dictionary = corpora.Dictionary.load(file_path)
        file_path = "%s/%s.tfidf.similarity.model" % (path, filename)
        model._similarity_model = similarities.SparseMatrixSimilarity.load(file_path)
        timer.stop()
        return model

    def similarities(self, text: Union[str, List[str]], top_n: int) -> List[Tuple[int, float]]:
        if not isinstance(text, list):
            text = text.split()

        timer = Timer("generating similarities from TF-IDF for the top n [%d] items" % top_n)

        inferred_vector = self._dictionary.doc2bow(text)
        self._similarity_model.num_best = top_n
        indexed: List[Tuple[int, float]] = self._similarity_model[self._model[inferred_vector]]
        sims: List[Tuple[int, float]] = [(self._index2id[idx], sim) for (idx, sim) in indexed]

        timer.stop()
        return sims

    def similarity(self, a: Union[str, List[str]], b: Union[str, List[str]]) -> float:
        if not isinstance(a, list):
            a = a.split()
        if not isinstance(b, list):
            b = b.split()
        timer = Timer("generate similarity from TF-IDF between the two items")

        vec_tfidf1 = self._model[self._dictionary.doc2bow(a)]
        vec_tfidf2 = self._model[self._dictionary.doc2bow(b)]

        sim = matutils.cossim(vec_tfidf1, vec_tfidf2)
        timer.stop()
        return sim
