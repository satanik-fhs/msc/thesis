from copy import deepcopy
from typing import Any, Dict, Iterable, List, Tuple, Type, Union

from django.db import connection
from django.db.models import QuerySet, Count, Case, When, IntegerField
from inflection import underscore

from app.blackbox.models import BaseModel, Corpus, TextModel
from app.models import Post, User
from app.utils import Timer, cache


class UserModel(BaseModel):

    def __init__(self, match_users: Dict[str, Any], match_items: Dict[str, Any], model_type: Type[TextModel],
                 method: TextModel.Method):
        super().__init__(model_type, method)
        self.__match_users: Dict[str, Any] = deepcopy(match_users)
        self.__match_items: Dict[str, Any] = deepcopy(match_items)

    def train(self):
        sql = "SELECT u.id" \
              ", COUNT(p.id) AS num_posts" \
              ", COUNT(IF(p.post_type_id = 1, 1, NULL)) AS num_questions" \
              ", COUNT(IF(p.post_type_id = 2, 1, NULL)) AS num_answers " \
              "FROM recs_user u " \
              "INNER JOIN recs_post p ON u.id = p.owner_user_id"

        where_items, having_items = {}, {}

        for key, val in self.__match_users.items():
            (where_items, having_items)[key in ["num_posts", "num_questions", "num_answers"]][key] = val

        params = []

        def make_clause(i: int, key: str, val: Union[Any, Dict[str, Any]]) -> str:
            clause = "\n"
            if i != 0:
                clause += "AND "
            clause += "(%s " % key
            clause += val["op"] if isinstance(val, dict) else "="
            clause += " %s)"
            params.append(val["val"] if isinstance(val, dict) else val)
            return clause

        if len(where_items) > 0:
            sql += "\nWHERE"
        for i, (key, val) in enumerate(where_items.items()):
            sql += make_clause(i, "u.%s" % key, val)

        sql += "\nGROUP BY u.id"

        if len(having_items) > 0:
            sql += "\nHAVING"
        for i, (key, val) in enumerate(having_items.items()):
            sql += make_clause(i, key, val)

        with connection.cursor() as cursor:
            cursor.execute(sql, params)
            ids = list(map(lambda t: t[0], cursor.fetchall()))

        data = User.objects.filter(id__in=ids)

        timer = Timer("gathering data for user model generation")
        fetched: List[User] = cache.get("users", lambda d: list(d), data)
        timer.stop()

        def create_posts(users: List[User]) -> List[Post]:
            user_ids: List[int] = list(map(lambda u: u.id, users))
            query: QuerySet = Post.objects.filter(user__id__in=user_ids)
            for key, val in self.__match_items.items():
                query = query.filter(**{underscore(key): val})

            return list(query)

        def create_user_posts(users: List[User]) -> List[Dict[str, Union[int, str]]]:
            posts: List[Post] = cache.get("users_posts", create_posts, users)

            user_bodies: Dict[int, str] = {}
            for post in posts:
                old: str = user_bodies.setdefault(post.user_id, "")
                user_bodies[post.user_id] = "%s %s" % (old, post.body)

            return [ { 'id': user_id, 'body': body } for user_id, body in user_bodies.items() ]

        name: str = "user_post_bodies"
        user_posts: List[Dict[str, Union[int, str]]] = cache.get(name, create_user_posts, fetched)

        timer = Timer("creating corpus for user model generation")
        corpus = Corpus(user_posts, "body", False)
        timer.stop()

        self._model.train(corpus)

    def _text_from_user(self, user: User) -> List[str]:
        timer = Timer("get posts per sql")
        query: QuerySet = user.posts
        for key, val in self.__match_items.items():
            query = query.filter(**{underscore(key): val})
        posts: QuerySet = query.all()
        bodies: Iterable[str] = map(lambda p: p.body, posts)
        timer.stop()
        words: List[str] = Corpus.lemmatize(" ".join(bodies))
        return words

    def similarities(self, user: User, top_n: int) -> List[Tuple[int, float]]:
        similarities = self._model.similarities(self._text_from_user(user), top_n)
        return similarities

    def similarity(self, a: User, b: User) -> float:
        similarity = self._model.similarity(
            self._text_from_user(a),
            self._text_from_user(b)
        )
        return similarity
