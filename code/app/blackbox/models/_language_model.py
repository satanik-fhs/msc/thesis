import re
from typing import List, Type, TypeVar

import numpy
import spacy
from spacy.attrs import ENT_TYPE, IS_ALPHA, LOWER, POS
from spacy.lang.en import English
from spacy.language import Language
from spacy.tokens import Doc, Token
from textpipe import doc

from app.utils import Logger, Timer

T = TypeVar('T', bound='LanguageModel')
logger = Logger.get()


class LanguageModel:
    __create_key = object()

    instance: T = None

    def __init__(self, create_key):
        assert (create_key == LanguageModel.__create_key), \
            "OnlyCreatable objects must be created using OnlyCreatable.create"
        timer = Timer('loading language model "en_core_web_lg" from spacy.io')
        try:
            self.nlp = spacy.load("en_core_web_lg", disable=['parser', 'ner'])
        except IOError as e:
            raise AssertionError("Please download spacy model with: python -m spacy download en_core_web_lg")
        timer.stop()

    @classmethod
    def create(cls: Type[T]):
        if not cls.instance:
            cls.instance = cls(cls.__create_key)

    @classmethod
    def get(cls: Type[T]) -> Language:
        cls.create()
        return cls.instance.nlp

    @classmethod
    def preprocess_text(cls: Type[T], text: str) -> Doc:
        try:
            text.encode("utf-8")
        except UnicodeEncodeError as e:
            if e.reason == 'surrogates not allowed':
                text = text.encode('utf-8', "backslashreplace").decode('utf-8')
        nlp: English = LanguageModel.get()
        filtered = doc.Doc(text)
        corpus: Doc = nlp(filtered.clean.lower())
        indices: List[int] = []
        for index, token in enumerate(corpus):
            token: Token = token
            if token.is_stop \
                    or token.is_punct \
                    or token.like_num \
                    or re.fullmatch(r'\s+', token.lemma_) \
                    or re.fullmatch(r'\'', token.lemma_) \
                    or re.fullmatch(r'\S*@\S*\s?', token.lemma_) \
                    or re.fullmatch(r'http\S+', token.lemma_) \
                    or token.pos_ not in ['NOUN', 'ADJ', 'VERB', 'ADV']:
                indices.append(index)
        np_array = corpus.to_array([LOWER, POS, ENT_TYPE, IS_ALPHA])
        np_array = numpy.delete(np_array, indices, axis=0)
        corpus2 = Doc(corpus.vocab, words=[t.text for i, t in enumerate(corpus) if i not in indices])
        corpus2.from_array([LOWER, POS, ENT_TYPE, IS_ALPHA], np_array)
        return corpus2
