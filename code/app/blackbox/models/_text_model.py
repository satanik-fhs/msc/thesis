import abc
from enum import Enum
from typing import List, Tuple, Union

from app.blackbox.models._corpus import Corpus


class TextModel(abc.ABC):

    class Method(Enum):
        pass

    @abc.abstractmethod
    def __init__(self, method: Method):
        pass

    @abc.abstractmethod
    def train(self, corpus: Corpus):
        pass

    @abc.abstractmethod
    def save(self, filename: str, path: str = None):
        pass

    @classmethod
    @abc.abstractmethod
    def load(cls, filename: str, path: str = None):
        pass

    @abc.abstractmethod
    def similarities(self, text: Union[str, List[str]], top_n: int) -> List[Tuple[int, float]]:
        pass

    @abc.abstractmethod
    def similarity(self, a: Union[str, List[str]], b: Union[str, List[str]]) -> float:
        pass
